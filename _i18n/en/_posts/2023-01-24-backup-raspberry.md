---
layout: post
title:  "Backup Raspberry"
date:   2023-01-24
tags: notes raspberry backup
---

# Backup Raspberry

This should be the last step of the initial setup of the Raspberry. After this, I can start adding new services and play with it safely. The other two things I did are:
- [setting up a good monitoring system using netdata](https://al.caprar.xyz/en/2023/01/21/monitoring-raspberry.html)
- [setting up access from outside the local network with CloudFlare tunnel](https://al.caprar.xyz/en/2023/01/23/accessing-raspberry-from-outside-without-opening-ports.html)

The tool I am considering for backup is autorestic. I've never used it personally but I got good reviews from colleagues and friends and there are many good articles online.

The destination of the backup will be [backbaze B2](https://backblaze.com). Also in this case, I've never used it but it seems very cheap and reliable.

I started following their installation [guide](https://autorestic.vercel.app/installation) that has a one-line command.

When I tried to run it (`wget -qO - https://raw.githubusercontent.com/cupcakearmy/autorestic/master/install.sh | bash`) I got:

```
linux
arm64
/usr/local/bin/autorestic.bz2: Permesso negato
bzip2: Can't open input file /usr/local/bin/autorestic.bz2: No such file or directory.
chmod: impossibile accedere a '/usr/local/bin/autorestic': File o directory non esistente
bash: riga 47: autorestic: comando non trovato
Successfully installed autorestic
```

I could not understand what was going on and I tried to replicate what the [install.sh](https://github.com/cupcakearmy/autorestic/blob/c82db56069dcf519a680d24b635263d6b6e8bc3f/install.sh) is doing:
```
wget https://github.com/cupcakearmy/autorestic/releases/download/v1.7.5/autorestic_1.7.5_linux_arm64.bz2
bzip2 -fd autorestic_1.7.5_linux_arm64.bz2
./autorestic_1.7.5_linux_arm64 install
```

The last command failed:
```
Downloading: https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
os.Rename() failed (rename /tmp/autorestic-3881862718 /usr/local/bin/restic: permission denied), retrying with io.Copy()
Error: open /usr/local/bin/restic: permission denied
cannot lock before reading config location
```

and I had to run it with `sudo`:
```
sudo ./autorestic_1.7.5_linux_arm64 install
Downloading: https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
Successfully installed 'restic' under /usr/local/bin
```

I thought it was installed, but it seems it installed only `restic`. By having a better look at the install.sh, I missed one step: to move the `autorestic` binary into `/usr/local/bin`: `sudo mv autorestic_1.7.5_linux_arm64 /usr/local/bin/autorestic`.

By now I think I have everything installed, I created a `.autorestic.yml` and run: `autorestic check -v`

```
Using config paths: . /home/alcaprar autorestic
Using config: 	 /home/alcaprar/autorestic/.autorestic.yml
Using lock:	 /home/alcaprar/autorestic/.autorestic.lock.yml
> Executing: /usr/local/bin/restic check
Initializing backend "backblaze"...
> Executing: /usr/local/bin/restic init
Error: exit status 1
```

At this stage, I think I messed up with `sudo` before and I will try to install them into `/home/USER/bin` instead of `/usr/local/bin`. From your home:
```
wget https://github.com/cupcakearmy/autorestic/releases/download/v1.7.5/autorestic_1.7.5_linux_arm64.bz2
bzip2 -fd autorestic_1.7.5_linux_arm64.bz2
chmod +x autorestic_1.7.5_linux_arm64.bz2
mv autorestic_1.7.5_linux_arm64.bz2 ./bin/autorestic
wget https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
bzip2 -fd restic_0.15.0_linux_arm64.bz2
chmod +x restic_0.15.0_linux_arm64.bz2
mv restic_0.15.0_linux_arm64.bz2 ./bin/restic
```

At this point I re-tried `autorestic check -v` but I still got the same output :(. I then came across this [comment on an issue](https://github.com/cupcakearmy/autorestic/issues/147#issuecomment-1135094548) that says that the backup command emits the output of the underlying `restic` command. And I tried it (`autorestic backup -a -v`), and discovered I had a typo in the config. 

I quickly fixed it, then executed `autorestic check -v` which worked like a charm and then `autorestic backup -a`. It worked!!

For today that's it. It was pretty simple to set it up (apart from some little problems). The next thing to do is to make it recurrent.