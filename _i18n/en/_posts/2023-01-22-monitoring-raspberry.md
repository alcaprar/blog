---
layout: post
title:  "Monitoring the new Raspberry"
date:   2023-01-21
tags: notes raspberry
---

# Monitoring the new Raspberry

The first thing I want to do before I start installing some software is to have a good monitoring system. I had a very good experience with [netdata](https://learn.netdata.cloud/) that comes already installed with CapRover ([I am running a CapRover instance on a DigitalOcean droplet](https://al.caprar.xyz/en/2022/08/20/my-self-hosted-setup.html)) and I want to use it also for my new Raspberry.

I've installed it following their official guide ["Monitor Pi-hole (and a Raspberry Pi) with Netdata"](https://learn.netdata.cloud/guides/monitor/pi-hole-raspberry-pi): 
- One line command: `wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --stable-channel`.
- Access it from `http://RASP_IP:19999/` and voilà, it already works!

By default the sensors data are not collected and I had to manually enable them. I am curious to see how the temperature fluctuate depending on the load.

1. Open the config to edit it
```
cd /etc/netdata
sudo ./edit-config charts.d.conf
```
2. Uncomment `sensors=force` and save it (in nano `ctlx + x` to exit. It will ask to save it.)
3. Restart it: `sudo systemctl restart netdata`

And after some minutes also the sensors data start to show up!


![Sensors](/assets/2023-01-22/sensors.png)