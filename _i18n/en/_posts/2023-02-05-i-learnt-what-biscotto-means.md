---
layout: post
title:  "Today I learnt what biscotto means"
date:   2023-02-05
tags: notes random food day-trip
---

# Today I learnt what biscotto means

Today I felt like an idiot and terribly satisfied when I learnt what _biscotto_ (italian word for cookie) means: _bis-cotto_ -> cooked two times.

I discovered it quite randomly when I was looking for "tipical food in Novara" during a last minute Sunday trip. And [this](https://www.thegretaescape.com/blog/prodotti-cucina-tipica-novarese/) is the page that made me a better person.