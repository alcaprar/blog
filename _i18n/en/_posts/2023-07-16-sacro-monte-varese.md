---
layout: post
title:  "Sacro Monte di Varese"
date:   2023-07-16
tags: hiking trekking lago-varese animals
---

Yesterday we went for a nice hike close to Varese, from [Sacro Monte di Varese](http://www.sacromontedivarese.it/) to Galvirate passing by Forte di Orino.

We took the train from Milan to Varese, then a local bus (I think line `C`) until the [1st Chapel](https://osm.org/go/0CklE0GYA?m=).

From here we followed a mix of these 3 itineraries found on Wikiloc:
- [Sacro Monte di Varese - Monte tre croci - Osservatorio](https://it.wikiloc.com/percorsi-escursionismo/sacro-monte-di-varese-monte-tre-croci-osservatorio-45970230)([KML](/assets/2023-07-16/sacro-monte-di-varese-monte-tre-croci-osservatorio.kml))
- [Da pensione Irma a Forte di Orino](https://it.wikiloc.com/percorsi-escursionismo/da-pensione-irma-a-forte-di-orino-62394231)([KML](/assets/2023-07-16/da-pensione-irma-a-forte-di-orino.kml))
- [Gavirate - Forte di Orino Sentiero 13 /313](https://it.wikiloc.com/percorsi-escursionismo/gavirate-forte-di-orino-sentiero-13-313-92246172)([KML](/assets/2023-07-16/gavirate-forte-di-orino-sentiero-13-313.kml))

## Animals

![Little animal #1](/assets/2023-07-16/DSC_0918.JPG)

![Little animal #2](/assets/2023-07-16/DSC_0920.JPG)

![Little animal #3](/assets/2023-07-16/DSC_0923.JPG)

![Little animal #4](/assets/2023-07-16/DSC_0927.JPG)

## Trees crushed by storm Alex

It was very sad to see the devastation made by [storm Alex](https://www.varesenews.it/2023/02/due-ottobre-2020-la-tempesta-alex-devasta-le-valli-del-varesotto-serata-analizzare-cose-accaduto/1548978/) about 3 years ago.

![Trees crushed by storm Alex](/assets/2023-07-16/DSC_0926.JPG)