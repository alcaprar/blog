---
layout: post
title:  "No more disk space"
date:   2023-02-02
tags: notes raspberry disk
---

# No more disk space

Today I was about to read my news from the RSS but an empty page with some errors showed up. I then ssh to the PI and quickly discovered the disk space was running out.

Some commands to find biggest offenders:
- Disk space human readable: `df -h`
- 40 biggest files in this folder and subfolders: `du -ah . | grep -v "/$" | sort -rh | head -n 40`
- 10 biggest folders from root folder: `sudo du -aBm / 2>/dev/null | sort -nr | head -n 10`
- systemd journal logs not used anymore: `journalctl --disk-usage`
- List of installed packages: `apt list --installed`

Some commands to clear some space:
- Clear apt cache: `sudo apt-get clean`
- Remove packages not used: `sudo apt-get autoremove`
- Remove docker things not used by any active container (run it only when your stack is up and running): `docker system prune -a -f`
- Remove some journal logs: `sudo journalctl --vacuum-time=3d`

After this quick exercise this is the result:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  6,0G    645M  91% /
```