---
layout: post
title:  "More privary, a reflection"
date:   2023-02-19
tags: notes privacy
---

# More privacy, a reflection

A couple of years ago, or maybe more, I started to care a bit more about my online privacy. Before that, I did not care at all and I actually enjoyed all the recommendations given by the various websites. Also, I had accounts everywhere and published content was often public. And I was relying a lot on Google.

At some point, I decided that it was time to change and with this post, I want to look back and see what I changed. 

I still have a lot to do since I still use a lot of Google services. I will try to think about what I can still do in another post.

## Browser

I was a big fan of Google Chrome because it's super integrated with the Google account and overall suite.

On my laptop, I switched to Firefox using the [multi-containers extension](https://github.com/mozilla/multi-account-containers#readme). I try to segregate everything (one container for every site I am registered with "Visit this website in this container" rule) and for random browsing, I always use a temporary container.

On my phone, I  switched to:
- [Firefox focus](https://www.mozilla.org/it/firefox/browsers/mobile/focus/) for almost all my need
- Firefox for those websites that require a login and it's convenient to keep the session open

## Search engine

I was relying on Google (as 84% of the global users, as of [December 2022](https://www.statista.com/statistics/216573/worldwide-market-share-of-search-engines/)) as my default and only search engine.

I am now using [Duckduckgo](https://duckduckgo.com/) for 95% of my searches. In some cases, I still fall back to Google because unfortunately, Google is too good at it.

## Password manager

Like the previous points, I used to use the Google password manager that integrates well between Google Chrome and the android phone.

I am now a happy paying customer of [Bitwarden](https://bitwarden.com/) and I share it with my family (you have up to 5 people with the family plan).

I have to say that it was easier than expected. On the laptop, I use the Firefox extension and on the phone their app. They both work well with an experience that is very similar to the previous one.

## News

I did not rely on a single service but I was using a mix of them: Google news, Feedly, newspapers pages on social media, newspapers websites...

I gave up on the applications and direct access to the websites. I am now using [Miniflux](https://miniflux.app/), an RSS reader, and I self-host it. I can't think of a world without it now.

## Android keyboard

I used to use the [Gboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin&gl=US), the Google keyboard. As you can imagine, this sends tons of info to Google and it's easily matched to the account. It's actually one of the best way for Google to build your profile.

I now use [OpenBoard](https://github.com/openboard-team/openboard) and I am pretty happy with it. It has also custom dictionaries and you can easily switch between languages. 

A negative side of it, but now I am used to not having it, is the lack of swipe-to-type. Before switching to this new keyboard I was using quite a lot that feature.

## Maps

I haven't really replaced Google maps but I started to use quite a lot [Organic Maps](https://organicmaps.app/it/) that uses [Open street maps](https://www.openstreetmap.org/).

I believe this will be very difficult to fully adopt given the features of Google maps: traffic alerts, public transport timetables (in some cases also live ones) and stores information.

The very positive side of using Organic Maps is the offline maps.

## Google history

I used to have all my history enabled in all Google services: my locations history, youtube search history, google results and so on.

I disabled it everywhere in order to reduce the recommendations based on location and past data.

I believe that it works well. When I open Youtube on my phone, where I am logged in with my Google account, the homepage looks quite random and the videos suggested are not really the ones I would watch.

## Facebook & Instagram

I still have these accounts but I am working towards closing them.

So far I marked all my content as private and removed all the pictures uploaded to both (and stored in my photo archive).

At the moment I think I won't close the Facebook account because I still use from time to time to buy/sell second-hand. It has a very good reach and a lot of items.

I have removed both applications on my phone and don't keep the session open in the browser so that I am not tempted to waste my time scrolling. Whenever I need something from them I open a new Firefox focus temporary page, login in, do what I wanted to do and close the tab (logging out then).


