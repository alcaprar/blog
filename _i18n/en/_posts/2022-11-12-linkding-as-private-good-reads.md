---
layout: post
title:  "How I am trying to use linkding as a private Good Reads alternative"
date:   2022-11-12
tags: self-hosted linkding good-reads
---

# How I am trying to use linkding as a private Good Reads alternative

A couple of weeks ago I stumbled across this [article](https://cri.dev/posts/2021-04-05-currently-self-hosting-apps/) from [cri.dev](https://cri.dev) where he lists the tools that he is currently self-hosting.

One of them caught my attention: [linkding](https://github.com/sissbruecker/linkding). I had never heard of it before and when I read it I had to google what it was about. The description in its GitHub repo says: `linkding is a simple bookmark service that you can host yourself. It's designed be to be minimal, fast, and easy to set up using Docker`.

This intrigued me because it was indeed something I was missing. I used to store bookmarks, links, collections and books (read and to be read) in a table in my [nocodb](https://www.nocodb.com/) self-hosted instance. It worked well but it was a bit too nerdy and cumbersome so I gave linkding a try.

I installed it with a couple of clicks in my CapRover server (if you want to know more about my self-hosted setup read [My self-hosted setup](https://al.caprar.xyz/en/2022/08/20/my-self-hosted-setup.html)) and I was immediately convinced that it's exactly the tool that I was missing.

With some manual work, I migrated all the content from nocodb to linkding. 

The part that I currently like the most is the catalog, mainly for books but it works well also for videos, and movies...: for each book, I add a new bookmark where:
- the `URL` is any page that describes it (Wikipedia page, Good Reads one...); 
- the `title` is the book's name; 
- the `description` contains my review/notes that I keep for every book. 

I then use the `tags` to organize them:
- each bookmark has the `libri` tag (it means `books` in Italian)
- the bookmark can have one of these: `da-iniziare` (`to-start`), `in-corso` (`in-progress`), `finito` (`done`).

With this categorization, I can quickly filter the books and find notes of old ones (`#libri finito`) or the list of the ones I am planning to buy/read (`#libri #da-iniziare`).
