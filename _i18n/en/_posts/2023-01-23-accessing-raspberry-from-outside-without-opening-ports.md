---
layout: post
title:  "Access the Raspberry from outside without opening ports"
date:   2023-01-23
tags: notes raspberry
---

# Access the Raspberry from outside without opening ports


I already heard of [Cloudflare tunnel](https://www.cloudflare.com/it-it/products/tunnel/) but I wanted to explore other alternatives. This is what I found:
- [loophole](https://loophole.cloud/)
- [zerotier](https://www.zerotier.com/) (from this [reddit thread](https://www.reddit.com/r/selfhosted/comments/s15scj/zerotier_vs_cloudflare_tunnelngrokalternatives/))
- [list of tunneling services](https://github.com/anderspitman/awesome-tunneling)

I decided to go for the easiest and most reliable solution, even if it's a closed source: Cloudflare tunnel. I believe it'll be easy to install and manage it. I will give it a go for a period and I will then evaluate the choice.

I tried to play with their dashboard directly but I got a bit lost and found this [guide](https://pimylifeup.com/raspberry-pi-cloudflare-tunnel/) ([archived page](https://web.archive.org/web/20221216160856/https://pimylifeup.com/raspberry-pi-cloudflare-tunnel/)).

Before I actually start running any commands I have to do these two things:
1. Add a website to Cloudflare
2. Change domain nameservers to point to Cloudflare

Point 1 was easy, just followed the wizard. Point 2 requires more time because nameservers updates can take up to 24 hours.

When the nameservers were set up I fixed all the DNS and then started to follow the above guide. Everything worked perfectly, apart from one thing: when I open the tunneled domain it did not work :(.

After trying different configurations I ended up fixing it. I believe that I had to remove the `proxied` setting in the DNS settings and leave it to `DNS only`.

Now I want to add the authentication in front of it using the Zero trust tool. It is as simple as adding a new application, creating a new policy and linking it to a domain. 