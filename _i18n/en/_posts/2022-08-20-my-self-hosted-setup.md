---
layout: post
title:  "My self-hosted setup."
date:   2022-08-20
tags: self-hosted caprover
---

# My self-hosted setup

2 years ago I started to get more and more into self-hosted and I tried a few setups before I found the current one that works pretty well.

When deciding if I am satisfied with that I use the following criteria:
- Is it (easily) repeatable?
- Is it simple to use?
- Do I have enough skills to operate it? If not, would it be too difficult (or too long) to learn that I'd give up?

With those in mind I've ended up:
-  using [DigitalOcean](https://m.do.co/c/9c49b1b21873) as a provider and using their cheapest droplet that costs me ~5/6€ per month and gives me enough power to run the few services I have at the moment. The main benefit of using DigitalOcean is that is just too simple and does not require any additional services to have a simple server up and running (firewalls, ingress, load balancer...).
- using [CapRover](https://caprover.com/) as a PaaS so that I don't have to manage `docker-compose` (actually, swarm) and the `ngnix` in front of it. Also, it comes with many [one-click apps](https://github.com/caprover/one-click-apps/tree/master/public/v4/apps) that you can install with exactly one click. And, last but not least, it integrates with [let's encrypt](https://letsencrypt.org/) and with the click of a button you get free HTTPS. I might write something more on CapRover because I just love it so much.
- buying the domain on [namecheap.com](namecheap.com) (it costs me ~10€ per year) but I don't use their DNS (I used this [guide](https://www.namecheap.com/support/knowledgebase/article.aspx/10375/2208/how-do-i-link-a-domain-to-my-digitalocean-account/) to link it). 

You can try to replicate it just by buying a cheap domain and creating a new droplet in [DigitalOcean](https://m.do.co/c/9c49b1b21873). When creating it you can choose one that has already [CapRover installed and configured](https://marketplace.digitalocean.com/apps/caprover). In a matter of a few minutes you'll be able to deploy your first service.