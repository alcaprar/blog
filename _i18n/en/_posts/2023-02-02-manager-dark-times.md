---
layout: post
title:  "Manager in dark times"
date:   2023-02-02
tags: notes management 
---

# Manager in "dark" times

Last year in these days I was announced that my internal interview to become an Engineering Manager was successful! And a few days later I officially started it with a team of 3 engineers (that used to be my colleagues before).

If I look back to last year and compare it to the last months (from December more or less) I see one big difference: the budget is not infinite anymore! Last year it seemed really infinite: generous salary increases to everybody, promotions, offsites, perks, conferences, business trips and so on. 

This was indeed a huge help to my job as a first-time manager. I haven't had to say no to almost any request and I was able to retain the people in my team thanks to £££ (the market was crazy hot until it [started chilling in April](https://newsletter.pragmaticengineer.com/p/the-scoop-8?s=w). In my company it was very good until the end of the summer.).

Now things are the complete opposite. Salary increases will be small, if not any; promotions will be scrutinized by a committee very diligently; business trips will require senior leadership approval; and more.

I believe that this is the real reality and last year it was just an insane bubble that was too good to be true. Let's enjoy, and learn, from the real world.