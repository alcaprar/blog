---
layout: post
title:  "Solved: add a task in the board using Jira automation "
date:   2022-08-24
tags: jira
---

# Solved: add a task in the board using Jira automation

When you start using Jira automation you enter a new world of endless possibilities. You start building workflows and process in order to solve Jira’s limits (yea, I hate Jira) and you think you can make literally everything.

My team needs to do a recurrent task every 2 weeks. We used to have a Slack reminder in our group that sent something like `Remember to do this 🤓 `. It worked for some times but then we realized that if we started to send a lot of messages in the group that message was forgotten and we forgot to do it. So it was not 100% reliable this process.

One day I started to play a bit more with Jira automation with the goal of solving this problem: I wanted to automatically add a new task to our board every 2 weeks so that we never forget to do it anymore. After the first test I realized that the new task was going to the `backlog` and I could not find a way to move it into the `board`. Googling a bit confirmed that I was not alone ([forum request](https://community.atlassian.com/t5/Jira-Software-questions/Jira-Automation-Adding-new-issue-to-board/qaq-p/1472581) and actual [feature request](https://jira.atlassian.com/browse/JSWCLOUD-21481) that was never implemented) and the workaround listed there were too complex and cumbersome.

At the end I came up with this simple solution:
- create a new task in the `board` and call it “Recurrent tasks container. Never close this task”.
- use Jira automation to add the new tasks into this container so that they will show up in the board.
- send a Slack message with the link to the task

The final workflow in Jira is composed of these steps:
1. the [trigger step](#trigger-step) (`When: Scheduled`) that defines the CRON expression.
  - Important: you need to tick the `Run a JQL search and execute actions for each issue in the query.`. In the JQL you need to add the ID (`id = XXX-1234`) of the ticket that will act as a container. Also you need to unselect the `Only include issues that have changed since the last time this rule executed` option.
2. [Create task step](#create-task-step) (`Then: Create a new`)
  - You need to select the project where it should be created and `Issue type = Subtask` and `Parent task = Current issue`.
3. A [`Branch rule / related issues` step](#branch-rule-step) to use the info of the task just created selecting `Most recently created issue`.
4. Inside the branch you need to call the `Re-fetch issue data` otherwise this workflow does not know about the newly created task (is it weird, right?).
5. And finally the [`Send Slack message` step](#send-slac-message-step).
  - The `Webhook URL` you will find it in your Slack app configuration.
  - The message can be something like: {% raw %} `A new recurring task has been created: https://XXX.atlassian.net/browse/{{issue.key}} 🤓` {% endraw %}


## Trigger step

![Trigger step](/assets/2022-08-24/trigger-step.png)

## Create task step


![Create task step](/assets/2022-08-24/create-task-step.png)

![Create task step definition.png](/assets/2022-08-24/create-task-step-definition.png)

## Branch rule step

![Branch rule step](/assets/2022-08-24/branch-rule-step.png)


## Send slack message step

![Send slack message step](/assets/2022-08-24/send-slack-step.png)
