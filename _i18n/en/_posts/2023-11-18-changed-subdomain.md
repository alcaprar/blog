---
layout: post
title:  "Changed the domain of this website"
date:   2023-11-18
tags: cloudflare google domain
---

I decided to change the domain of this website and remove the `blog` word since it's not a blog but just a collection of random notes.

It was quite simple:
1. change all the setup in Jekyll
2. rename all occurrences in this repo so old links will work
3. add a new domain in the GitLab pages configuration
  3.1 verify the new domain: add a new TXT DNS in Cloudflare
4. add a new CNAME DNS in Cloudflare to point to GitLab
5. Add a bulk redirect rule in Cloudflare so everything from the old domain will be routed to the new one (following [this](https://developers.cloudflare.com/fundamentals/setup/manage-domains/redirect-domain/) - [archived](https://web.archive.org/web/20231024051634/https://developers.cloudflare.com/fundamentals/setup/manage-domains/redirect-domain/)). I've enabled all parameters: `Preserve query string`, `Include subdomains`, `Subpath matching`, `Preserve path suffix`.