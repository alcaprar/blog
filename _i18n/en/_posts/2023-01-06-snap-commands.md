---
layout: post
title:  "Snap commands"
date:   2023-01-06
tags: notes linux terminal
---

# Snap commands

- Install a package: `snap install <PACKAGE_NAME>`
- Info about an installed package `snap info <PACKAGE_NAME>`
- Update a package: `snap refresh <PACKAGE_NAME>`
- Update all: `snap refresh`
- Remove one package: `snap remove <PACKAGE_NAME>`