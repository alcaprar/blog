---
layout: post
title:  "Move Docker data on an external drive to free up memory on the main SD card"
date:   2023-03-30
tags: raspberry docker
---

# Move Docker data on an external drive to free up memory on the main SD card

## Attempt #1 - Failed

I followed these guides:
- https://thesmarthomejourney.com/2021/02/11/moving-docker-data-to-an-external-ssd/
- https://www.howtogeek.com/devops/how-to-store-docker-images-and-containers-on-an-external-drive/

First list external drives: `df`.

My output:
```
File system    1K-blocchi   Usati Disponib. Uso% Montato su
/dev/root         7303220 6419724    529608  93% /
devtmpfs          3834192       0   3834192   0% /dev
tmpfs             3999920     204   3999716   1% /dev/shm
tmpfs             1599968    1228   1598740   1% /run
tmpfs                5120       4      5116   1% /run/lock
/dev/mmcblk0p1     261108   31292    229816  12% /boot
tmpfs              799984     516    799468   1% /run/user/1000
/dev/sda1       960302096      28 911447644   1% /media/alcaprar/lacie
```

Then create a folder where we'll mount the drive: `sudo mkdir /mnt/ext-drive`

Mount the drive on the new folder: `sudo mount /dev/sda1 /mnt/ext-drive/`. This is a temporary mount.

Make the mount permanent:
1. Find the drive UUID: `ls -l /dev/disk/by-uuid`
2. Edit `/etc/fstab` and add something similar to: `UUID=ef63c1cf-6d18-48c3-976d-f8590b8a662d /mnt/ext-drive ext4 defaults 0 0 `

Add (or change if it exists already) to the `/etc/docker/daemon.json`:

```
{
    "data-root": "/mnt/ext-drive/docker-data"
}
```

IMPORTANT: Before proceeding do a backup!!!

Copy the previous docker data into the new folder in the external drive: `sudo rsync -aSv /var/lib/docker/ /mnt/ext-drive/docker-data`.

Copy the previous volumes data: `sudo rsync -aSv ~/self-hosted/data /mnt/ext-drive/self-hosted/data`.

Restart docker daemon: `sudo service docker restart`.

At this point everything should start up without problems and you should have all the data.

Now this is the new output of `df`:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  5,7G    1,1G  85% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  204K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M  516K    781M   1% /run/user/1000
/dev/sda1       916G  464M    869G   1% /mnt/ext-drive
```

A bit disappointed, only a 8% saving. I will dig into it a bit more in the future.


Since I moved the `/var/lib/docker` into the external drive I can remove it. This gave only 1% more of free space, better than nothing.

These are my 10 biggest folders: `sudo du -aBm / 2>/dev/null | sort -nr | head -n 10`
```
6761M	/
3065M	/usr
1677M	/home/alcaprar
1677M	/home
1605M	/home/alcaprar/.local/share/docker
1605M	/home/alcaprar/.local/share
1605M	/home/alcaprar/.local
1598M	/home/alcaprar/.local/share/docker/overlay2
1524M	/usr/lib
934M	/var
```

The `.local/share/docker` folder occupies still a lot of space. But I am not sure if it's still in use or not.

At this point I was not sure that the above worked. This [stackoverflow comment](https://stackoverflow.com/questions/24309526/how-to-change-the-docker-image-installation-directory#comment116088146_50217666) helped me to understand that it did not. If I run `docker info|grep "Docker Root Dir"` I still get `Docker Root Dir: /home/alcaprar/.local/share/docker` which is not the new folder I configured above.

## Attempt #2 - Failed again

I am now going to try this [guide](https://www.baeldung.com/ops/docker-image-change-installation-directory). This is what I did:
- `docker-compose down`
- backup `data` folder where I bind volumes
- backup the `/mnt/ext-drive/docker-data` that I thought Docker is now using: `sudo rsync -aSv  /mnt/ext-drive/docker-data /mnt/ext-drive/docker-data-backup`. And `sudo rm -r /mnt/ext-drive/docker-data && mkdir /mnt/ext-drive/docker-data`.
- change the `ExecStart` line in the service configuration file: `sudo vi /lib/systemd/system/docker.service` to be`ExecStart=/usr/bin/dockerd --data-root /tmp/new-docker-root -H fd:// --containerd=/run/containerd/containerd.sock`
- restart docker by running `sudo systemctl daemon-reload` and `sudo systemctl restart docker`. The restart failed and by running `journalctl -xe` I saw this error `service-start-limit-hit`. I found somewhere that the `.json` config could interfeer and I deleted it. After re-running the restart it worked.

At this point I retried the `docker info` with no luck. It still shows the old folder.

## Attempt 3

Let's start from the beginning and re-install it from scratch.

First of all let's wipe Docker: https://askubuntu.com/a/1021506.

Now I have a lot of free space finally!
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  3,8G    2,9G  58% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  408K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M   32K    782M   1% /run/user/1000
/dev/sda1       916G  2,1G    868G   1% /mnt/ext-drive
```

I used the get [script](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) provided in their installation page and after the installation this is the space situation:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  4,2G    2,5G  63% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  408K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M   32K    782M   1% /run/user/1000
/dev/sda1       916G  2,1G    868G   1% /mnt/ext-drive
```

And `docker info` still shows that root folder.

This time I will create a symlink instead of tweaking the configs:
1. Stop docker: `sudo systemctl stop docker.socket` and `sudo systemctl stop docker`
2. Since it's a fresh installation there is nothing to move.
3. Create the symlink: `ln -s /mnt/ext-drive/docker-data /home/alcaprar/.local/share/docker`
4. Restart docker. I wanted to be sure and I also rebotted.
5. Restart the `docker-compose` and wait that it downloads all the images again. Evreything is up and working!

And, `df -h` now shows this:

```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  4,1G    2,6G  62% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  204K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
/dev/sda1       916G  3,7G    866G   1% /mnt/ext-drive
tmpfs           782M  512K    781M   1% /run/user/1000
```

Finally `/` has remained the same and `/dev/sda1` has now `1.5 GB` more!