---
layout: post
title:  "Resize images in Linux terminal"
date:   2022-11-13
tags: notes linux terminal
---

# Resize images in Linux terminal

Install it:
- `sudo apt get install imagemagick`

Some useful ones:
- `convert example.jpg -resize 1000x800 example.jpg`: keep aspect ratio. The image will fit the given area. File overwritten.
- `convert example.jpg -resize 1000 example.jpg`: keep aspect ratio. The image will fit a width of 1000. File overwritten.
- `convert example.jpg -resize x800 example.jpg`: keep aspect ratio. The image will fit a height of 1000. File overwritten.
- `convert example.jpg -resize 1000x800! example_new.jpg`: force the image to become an exact size (even if it messes up the aspect ratio). A new file is created and the original is untouched.

[Source](https://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/)([internet archive](https://web.archive.org/web/20221027192723/https://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/)).