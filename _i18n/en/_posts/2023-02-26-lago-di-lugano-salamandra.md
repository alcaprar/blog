---
layout: post
title:  "Spotted a Salamandra"
date:   2023-02-26
tags: hiking trekking lago-lugano salamandra animals
---

# Spotted a Salamandra

Yesterday I went hiking in the mountains that surround the lake of Lugano. Together with a friend, we left Milano Porta Garibaldi by train and in 1 hour 20 minutes we reached Porto Ceresio.

From there we followed this nice [track](https://it.wikiloc.com/percorsi-escursionismo/monte-san-giorgio-dalla-stazione-di-porto-ceresio-97183837) from wikiloc (here the [KML](/assets/2023-02-26/monte-san-giorgio-dalla-stazione-di-porto-ceresio.kml) I used to follow it using [OrganicMaps](https://al.caprar.xyz/en/2023/02/19/more-privacy-reflection.html#maps) without Internet connection).

The view is beatiful along all the way and most of the path is under a nice forest. 

And we met this animal: Salamandra!

![Salamandra](/assets/2023-02-26/salamandra.jpg)

![Bosco](/assets/2023-02-26/bosco.jpg)

![Vista](/assets/2023-02-26/vista.jpg)