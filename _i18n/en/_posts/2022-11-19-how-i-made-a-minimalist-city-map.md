---
layout: post
title:  "How I created a minimalist city map poster"
date:   2022-11-19
tags: how-to art maps
---

# How I created a minimalist city map poster

Nowadays minimalist city map posters are quite common and you can see a ton of them online and in fancy shops, restaurants, etc. 

You can buy them online on a generic website like [Etsy](https://www.etsy.com/market/minimalist_city_map) or in specialized ones like [nativemaps.us](https://nativemaps.us/collections/all-maps) or [wijck.com](https://www.wijck.com/en/).

The con of Etsy or similar is that you can't customize it and if you want a very specific city/area you can't choose. On the other hand, the other specialized sites offer you the possibility to choose any point on the map and therefor make a map also for the most unknown place on Earth. This comes with a price 💸.

I then said: "at the end of the day it's simply a minimal map plus some basic framing. There must be something online open and free.". And I was correct, a quick search on the Internet revealed me that with [snazzymap.com](https://snazzymaps.com)(you can apply alsmost any style to Google maps) and [canva.com](https://canva.com)(free design tool pefect for non-artist like me) I could have done it myself.