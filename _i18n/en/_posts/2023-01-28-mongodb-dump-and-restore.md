---
layout: post
title:  "Mongodb dump and restore"
date:   2023-01-28
tags: notes raspberry
---

# Mongodb dump and restore

Today I moved the last service from the droplet and it had a mongodb attached. To dump it and restore it in the Raspberry I did the following.

1. From the `mongodb` container, this command is for dump: `mongodump -d DB_NAME -o today`. It creates a folder and inside it there are a bunch of `.bjson` files.
2. Compress the folder: `tar -cvf today.tar ./today`
3. File.io ([the tool I used when moving the postgresql db](https://al.caprar.xyz/en/2023/01/25/install-docker-and-deploy-first-service.html)) said I had no more space available and I used [uguu](https://uguu.se/): `curl -F files[]=@today.tar https://uguu.se/upload.php`
4. Copy the file into the Raspberry
5. Extract the tar: `tar -xvf today.tar`
6. Start a new `mongodb` container with a volume attached.
7. Copy the folder into the volume attached
8. Restore the DB: `mongorestore --db DB_NAME FOLDER` (and I used `.` as `FOLDER`)
