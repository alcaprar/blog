---
layout: post
title:  "Can I restore the backups?"
date:   2023-06-03
tags: raspberry backup
---

In the last [post](https://al.caprar.xyz/en/2023/04/09/i-said-i-had-backups.html) I was arguing about wrong and empty backups.

Until a few days ago I was very happy about my Raspberry setup and the fact I spent time on backups and monitoring at the beginning of this adventure. Then I realized: "I said I had backups but last time it was not true. I fixed that but I never tried to restore them in another machine". And I was scared I could lost my data in every moment.

So I decided to try to restore the backup in my backblaze bucket into my personal laptop. When I wrote the other post I did restore it but on the Raspberry.

The first I tried was to install `autorestic` but for some reasons similar to the ones [here](https://al.caprar.xyz/en/2023/01/24/backup-raspberry.html) I could not do it. I really did not want to fight again and I remember they also offered a [Docker image](https://autorestic.vercel.app/docker) and I went with it.

At this point I realized that my backups were at very high risk: I did not save the key nor the `autorestic.yml` but I only had the access key for the bucket. But as they state in their [docs](https://autorestic.vercel.app/quick), without the key that data is useless since the encryption is done in the server:

![warning about data loss](/assets/2023-06-03/warning.png)

So the first thing I did was to `ssh` to the Raspberry and save the `autorestic.yml` into my [Bitwarden Vault](https://al.caprar.xyz/en/2023/02/19/more-privacy-reflection.html#password-manager).

Then I created a file called `autorestic.yml` on my personal laptop and then executed:

```bash
docker run --rm -v $(pwd):/data cupcakearmy/autorestic autorestic restore -l home -c /data/autorestic.yml --to /data/restore
```

This succesfully executed and I got my `restore` folder. I tried to `cd` but I got `access denied`. Oh fuck. A quick `ls -l` showed me that `root` owned that folder so I executed (thanks [Stackoverflow](https://askubuntu.com/a/720000) that I can always forget basic commands): `sudo chmod a+rwx restore` and tried again: I could access it and everything was there. Backup are safe.