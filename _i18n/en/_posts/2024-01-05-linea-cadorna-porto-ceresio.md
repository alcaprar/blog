---
layout: post
title:  "Linea Cadorna - Lugano's lake"
date:   2024-01-05
tags: hiking trekking lago lugano
---

Today I and my brother went for a day trekking next to Lugano's lake to see the trenches from the 2nd world war that were built to defend this part of Italy. They are still there and in good shape and it's a very interesting hike.

We took a train from Milano to Porto Ceresio (1 hour and 15 minutes) and from there we climbed up the Monte Casolo and then Monte Pravello. From here you just walk down until Monte Orsa in the middle of the trenches. There were also 2 big cannons and lots of tunnels.

[GPX recording of the day](/assets/2024-01-05/linea-cadorna.gpx).

Some pictures:

![Tunnel](/assets/2024-01-05/tunnel.jpg)
![Cannon](/assets/2024-01-05/cannon.jpg)
![Trench](/assets/2024-01-05/trench.jpg)
![View](/assets/2024-01-05/view.jpg)