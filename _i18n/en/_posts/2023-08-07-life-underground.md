---
layout: post
title:  "Life underground"
date:   2023-08-07
tags: life animals
---

Today I did some maintenance work in the garden and I had to move some tiles in order to remove some unwanted plants.

It was fascinating to see how much life exists underground!

## Ants

This was an old ants' house. Nobody showed up.

![Ants](/assets/2023-08-07/old_ants_house.JPG)

This is a very busy ants' house.

[Ants](/assets/2023-08-07/ants.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/ants.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## 2 future lizards

![Lizard eggs](/assets/2023-08-07/lizard_eggs.JPG)

## Naked snail

[Naked snail](/assets/2023-08-07/snail.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/snail.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## Run away

[Run away 1](/assets/2023-08-07/run-away1.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/run-away1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

[Run away 2](/assets/2023-08-07/run-away2.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/run-away2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 