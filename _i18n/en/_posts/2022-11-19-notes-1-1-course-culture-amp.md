---
layout: post
title:  "Notes from a CultureAmp course on 1:1 meetings"
date:   2022-11-19
tags: notes 1:1 management meeting
---

# Notes from a CultureAmp course on 1:1 meetings

CAMPS (Certainty, Autonomy, Meaning, Progress, Social inclusion) framework.

## Certainty

- Goals: What does success look like?
- Priorities: What matter most?
- Roles: What s in the scope of the job and future job progression?
- Decision status: How are things decided?
- ...

Other useful questions to build certainty:
- On a scale 1-10, how clear do you feel about [x y z]?
- What should we take away from this conversation?

## Autonomy

You have a choice, options, and that your voice matters

- Task: what they do
- Time: when they do it
- Technique: how they do it
- Team: Who they do tasks with

Useful questions for a project:
- What do you see as the next step?
- How would you like to go about this?
  - Instead of simply: we should do this. 
- As the project owner, what order do you want to do this in?

Useful questions for 1:1s:
- How satisfied are you with how "in charge" you feel of your work.
- Where would you like more or less freedom?

## Meaning

When we connect employees' work to things that matter to them they are more engaged, satisfied and productive.

Some questions:
- What's important to you about this?
- What do you see as the main purpose behind this project/task?

## Progress

Not the big wins that matter the most, but a feeling of continuous achievement.

Some questions:
- What did you learn from this?
- Tell me one thing you did well in the last week.
- How will you break this project up into phases?

## Social inclusions

Direct report should feel a sense of inclusion. Whenever we feel excluded, our brain registers it as if we were experiencing physical pain.

- Small talk: "Hey, you mentioned last weekend you were doing [x], how did it go?"
- Make sure your direct report feels they have the chance to contribute: "During our next team meeting, can you share [x y z]?"
- Doing a quick check on dynamics on the team-wide level: "How would you rate the quality of our team meetings?"
- Ask if they wanted to be connected with somebody in the company.