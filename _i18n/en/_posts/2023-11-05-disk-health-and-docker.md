---
layout: post
title:  "I wanted to know the HDD health and I killed Docker"
date:   2023-11-05
tags: raspberry docker hdd
---

Visto che sto usando un vecchio hard disk da 1TB (15 anni?) ogni tanto ho paura che mi possa lasciare da un momento all'altro. Oggi ho provato a vedere se c'era qualche soluzione per conoscere qualcosa di più su di lui e ho trovato questo [articolo](https://linuxconfig.org/how-to-check-an-hard-drive-health-from-the-command-line-using-smartctl)([archiviato](https://web.archive.org/web/20231027191117/https://linuxconfig.org/how-to-check-an-hard-drive-health-from-the-command-line-using-smartctl))  che parlava di [`smartctl`](https://linux.die.net/man/8/smartctl).

L'ho provato: dopo averlo installato con `apt`, ho lanciato: `sudo smartctl -i /dev/sda` ed è uscito:

```
smartctl 7.2 2020-12-30 r5155 [aarch64-linux-5.15.84-v8+] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Vendor:               SAMSUNG
Product:              HD103UJ
User Capacity:        1.000.204.886.016 bytes [1,00 TB]
Logical block size:   512 bytes
Serial number:        152D20329000
Device type:          disk
Local Time is:        Sun Nov  5 14:36:25 2023 CET
SMART support is:     Unavailable - device lacks SMART capability.
```

Purtroppo il mio vecchio hard disk non supporta SMART :(. A questo punto però qualcosa è successo, ma non so cosa. L'hard disk è diventato di sola lettura e tutti i container Docker hanno iniziato a fallire ([il mio Docker usa l'hard disk per salvare i suoi dati e non la SD](https://al.caprar.xyz/it/2023/03/30/docker-on-external-drive-to-free-up-sd-memory.html)). Riuscivo ad utilizzare i servizi ma qualsiasi azione che modificasse i dati falliva (dai log di un container ho visto qualcosa tipo `read-only file system`). 

Ho subito provato nel modo più semplice: spegnere e riaccendere i container. Ma `docker-compose down` non ha funzionato e ha sparato molte righe di log come queste:
```
ERROR: for self-hosted_mongodb_1  container da0f4865943a7c338660006e857dfada61b1fff08d1fb02585e10737f13a5270: driver "overlay2" failed to remove root filesystem: unlinkat /mnt/ext-drive/docker-data/overlay2/d893045b5f5181a43d51abe2989fbc627c89e8dedb7026606902086f6f399ff3: read-only file system
Removing network self-hosted_default
ERROR: error while removing network: network self-hosted_default id 114fecd466ee36774d0bef247ba8127dbab79c9ce25f091bae3a8d3480337546 has active endpoints
```

A questo punto avevo un po' paura di aver fatto danni. Non volevo riavviare il Pi perchè pensavo che Docker non fosse più ripartito e di conseguenza avrebbe bloccato l'avvio del Pi. Ho cercato di trovare situazioni simili online e [una delle risposte su Stackoverflow](https://stackoverflow.com/a/74207386/4696783) mi ha impaurito ancora di più. Il mio `dmesg` restituiva qualcosa come (non ci sono colori qui ma era tutto rosso nel terminale):
```
[23542.142493] EXT4-fs warning (device sda1): ext4_end_bio:348: I/O error 10 writing to inode 16129270 starting block 7139300)
[23542.142566] Buffer I/O error on device sda1, logical block 7139042
[23542.142588] Buffer I/O error on device sda1, logical block 7139043
[23544.614305] sd 0:0:0:0: [sda] tag#0 UNKNOWN(0x2003) Result: hostbyte=0x07 driverbyte=DRIVER_OK cmd_age=0s
[23544.614326] sd 0:0:0:0: [sda] tag#0 CDB: opcode=0x2a 2a 00 20 9d 4a b8 00 00 08 00
[23544.614332] blk_update_request: I/O error, dev sda, sector 547179192 op 0x1:(WRITE) flags 0x0 phys_seg 1 prio class 0
[23544.614343] EXT4-fs warning (device sda1): ext4_end_bio:348: I/O error 10 writing to inode 16129026 starting block 68397400)
[23544.614355] Buffer I/O error on device sda1, logical block 68397143
[23545.128173] sd 0:0:0:0: [sda] tag#0 UNKNOWN(0x2003) Result: hostbyte=0x07 driverbyte=DRIVER_OK cmd_age=0s
[23545.128194] sd 0:0:0:0: [sda] tag#0 CDB: opcode=0x2a 2a 00 3a 07 23 10 00 00 88 00
[23545.128200] blk_update_request: I/O error, dev sda, sector 973546256 op 0x1:(WRITE) flags 0x800 phys_seg 17 prio class 0
[23545.128263] Aborting journal on device sda1-8.
[23545.206305] sd 0:0:0:0: [sda] tag#0 UNKNOWN(0x2003) Result: hostbyte=0x07 driverbyte=DRIVER_OK cmd_age=0s
[23545.206331] sd 0:0:0:0: [sda] tag#0 CDB: opcode=0x2a 2a 00 3a 04 08 00 00 00 08 00
[23545.206339] blk_update_request: I/O error, dev sda, sector 973342720 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[23545.206351] Buffer I/O error on dev sda1, logical block 121667584, lost sync page write
[23545.206386] JBD2: Error -5 detected when updating journal superblock for sda1-8.
[23547.145991] EXT4-fs error (device sda1): ext4_journal_check_start:83: comm dockerd: Detected aborted journal
[23547.230310] sd 0:0:0:0: [sda] tag#0 UNKNOWN(0x2003) Result: hostbyte=0x07 driverbyte=DRIVER_OK cmd_age=0s
[23547.230338] sd 0:0:0:0: [sda] tag#0 CDB: opcode=0x2a 2a 00 00 00 08 00 00 00 08 00
[23547.230346] blk_update_request: I/O error, dev sda, sector 2048 op 0x1:(WRITE) flags 0x3800 phys_seg 1 prio class 0
[23547.230359] Buffer I/O error on dev sda1, logical block 0, lost sync page write
[23547.230400] EXT4-fs (sda1): I/O error while writing superblock
[23547.230408] EXT4-fs (sda1): Remounting filesystem read-only
[23584.896419] br-114fecd466ee: port 3(vethe8b1298) entered disabled state
[23584.896882] veth29119cf: renamed from eth0
[23585.006119] overlayfs: upper fs is r/o, try multi-lower layers mount
[23586.118680] br-114fecd466ee: port 11(veth362085a) entered disabled state
[23586.118897] vethff16379: renamed from eth0
[23586.224521] overlayfs: upper fs is r/o, try multi-lower layers mount
```

Ho provato ancora più volte a far ripartire i container ma senza fortuna. Ho anche provato a riavviare il servizio Docker (`sudo service docker restart`) ma sempre senza risultati. 

A questo punto ho rischiato il riavvio del Pi sperando per il meglio. E ha funzionato :) Appena ripartito tutto funzionava come prima e non c'erano più errori strani dovuti al file-system.

 When the Pi started, all the docker containers restarted and everything was back!