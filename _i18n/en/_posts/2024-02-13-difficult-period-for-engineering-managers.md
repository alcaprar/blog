---
layout: post
title:  "Difficult period for engineering managers"
date:   2024-02-13
tags: notes management
---

Almost exactly last year I wrote this note ["Manager in dark times"](https://al.caprar.xyz/en/2023/02/02/manager-dark-times.html). I was saying that it was not easy to be a manager when the overall market condition switched from being very positive to very negative.

A few months later I went back to IC (the decision that was _already made_ 3/4 months before) :).

Today I was reading an article ([The end of 0% interest rates: what the new normal means for engineering managers and tech leads](https://newsletter.pragmaticengineer.com/p/zirp-engineering-managers)) from Gergely Orosz and there are a few parts that I liked and I've seen it myself.

## Being a manager is harder than before

This really resonates well with the note linked above.

```
During the “good times,” of rapid growth, being a manager is rewarding. You hire lots of enthusiastic people, get existing engineers promoted, and when the work gets too much, another new joiner brings fresh energy to the team. Time flies when things are good.

Managing during a downturn is emotionally draining, with little support. When a company is doing poorly, the motivating parts of the job are fewer, like hiring, starting greenfield projects, and promoting high performers. Instead, there’s:

- Planning and executing a layoff handed down from above. We previously covered how to do these humanely
- Learning about layoffs as a line manager at the same time as everyone else
- Trying to restore morale after a downsizing
- Resignations on the team
- Low morale and fear of further cuts
- Having to put a brave face on for team mates affected by decisions you didn’t make
- Personal job worries, as manager positions are at higher risk than ICs

… all during lower pay rises, smaller bonuses, and less compensation than before
```

## Go back to IC

I would have probably done it anyway, also in an easier period, but I do really understand and see this a lot.

```
You could go back to being an IC, and act as a hands-on tech lead. This is a path I’m observing many former managers take who used to head up smaller teams. Several had titles like director of engineering or head of engineering, while working with small enough teams.
```

## Great engineers not becoming managers might be good

I am not saying I am a great engineer but I felt this _pressure_ to try the management role. I think I had some of the (soft) skills required to be a manager and I was encouraged to do so when the former manager left.

That probably would have not happened in this period and I'd have remained an engineer without this short stint as a manager.

```
Until recently, there was always pressure at Big Tech and scaleups for standout engineers to become managers. In an environment where teams grow fast, managers kept scouting for candidates to step up to manage a part of the team they didn’t have bandwidth for. Also, a manager with several managers reporting was en-route for a director promotion.

It was easy to get great engineers who were also solid communicators, to take roles. There was no real downside because reverting to an engineer was an option, while being a manager could open new career paths. However, many engineers-turned-managers didn’t go back to engineering, even if they didn’t enjoy the role.

Now, there will be much less pull for engineers to become managers, so some great engineers will probably decide to pass, which is not a bad thing. Engineers with more passion for helping others grow than writing code, will still go for it; and they’re the ones who tend to make great managers.
```