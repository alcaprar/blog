---
layout: post
title:  "Shutdown PI during the night"
date:   2023-06-20
tags: raspberry
---

The Raspberry PI is next to my working desk and has an old hard drive connected to it. The drive is quite noisy and you can clearly hear it during the morning and evening/night when there is silence.

Since I don't really use it much in those moments I thought I could automatically shut it down during the night, maybe keeping it up only from 9 AM to 9 PM.

Since the Raspberry cannot be turned on programmatically I'll be using an electrical timer that cuts off electricity outside of that window (the one you can see in this [Stackexchange answer](https://raspberrypi.stackexchange.com/a/120446)).

The last thing needed is a cron job that calls `shutdown` a bit before the electricity goes off so that it has enough time to properly shutdown.

I added it by calling `sudo crontab -e` (remember `sudo` because it has to go in the `root`'s crontab since `shutdown` requires `sudo`) and then I added: `0 21 * * *  /sbin/shutdown -h now`.