---
layout: post
title:  "I said I had backups"
date:   2023-04-09
tags: raspberry backup
---

# I said I had backups

When I first set up my raspberry I said I wanted to have backups working before I started deploying anything. And this is what I thought I did and I _explained_ it [here](https://al.caprar.xyz/en/2023/01/24/backup-raspberry.html).

I thought it was working fine, things were uploaded into the bucket and I was happy.

When a couple of weeks ago I [moved docker data to the external hard-drive](https://al.caprar.xyz/en/2023/03/30/docker-on-external-drive-to-free-up-sd-memory.html) in order to reduce the load of the SD card I fucked up something and I had to restore the DB of `miniflux` (it was the only one that got the DB corrupted). I thought I had daily snapshots so I'd have lost just some data, not too bad.

When I run `autorestic restore` and opened the `miniflux.sql` I found: `error while making the dump. Container not found.`. My reaction was "fuck me!".

This experience reminded me that you don't have a proper backup in place until you tested that you can quickly restore it. So next time I should have tried to restore it before calling it a victory!

Btw, now it works. I fixed the backup script and tried a restore. The data is there :) 