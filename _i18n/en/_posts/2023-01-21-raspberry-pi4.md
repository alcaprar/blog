---
layout: post
title:  "Raspberry - first day"
date:   2023-01-21
tags: notes raspberry
---

# Raspberry - first day

Today I unpacked the Raspberry PI 4 that I received for Christmas (yes, somebody is still able to find a good gift every Christmas :D). I used to have one when I was at uni and I had played a bit with it but not too much. I don't even remember what I used it for apart from a little experiment with OpenCV and a wifi camera to detect people entering the house.

I am very surprised and happy by the easiness of the installation of Raspbian. I started from this official [page](https://www.raspberrypi.com/software/), then only installed Raspberry PI imager (`sudo apt install rpi-imager`) and followed the easy wizard to flash the SD card (I am now using an old 8 GB card just for some initial testing, I am planning to replace it with a bigger one soon).

The SD flash took 5-10, then I simply inserted it into the Raspberry, connected a keyboard and a mouse and connected it to the TV. When turned it on it did some things for the first time setup and in a few minutes (well, it took a bit to install the updates) I was able to use it!

As of today, I've only enabled SSH (followed this [guide](https://www.howtogeek.com/768053/how-to-ssh-into-your-raspberry-pi/)) so I can start using it remotely without the need for a TV, mouse and keyboard.