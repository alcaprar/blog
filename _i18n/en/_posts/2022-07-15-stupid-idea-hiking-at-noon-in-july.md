---
layout: post
title:  "Hiking at noon in July is not a good idea"
date:   2022-07-15
tags: hiking trekking lago-como lago-lecco resegone
---

A few days ago I went with my brother for a 2-days hike to [Resegone mountain](https://it.wikipedia.org/wiki/Resegone), close to Lecco. We booked one night at [Rifugio Stoppani](https://www.rifugiostoppani.it/) in a shared room and we decided to get there without driving but using a train and legs.
We left Milano Centrale on board a Trenord train with a final stop at Lecco. From here we walked to [Acquate](https://goo.gl/maps/7C9WWcGv3JPtdJNK7) and then followed the [guide](https://www.rifugiostoppani.it/percorso-da-acquate.html) available on the _rifugio_'s website. Acquate is a neighborhood of the city of Lecco and it's famous for having been the environment for many scenes of the [Promessi sposi](https://movio.beniculturali.it/dsglism/IpromessisposiinEuropaenelmondo/it/65/edizione-inglese) and the native city of Renzo and Lucia (the main characters of the novel).

The first part, from Lecco's train station to Acquate was terrible: little shadow and lots of buildings and streets that were releasing so much heat. And, it was uphill. We went so slow and at every step, we regretted the decision to leave at noon. The thermometer was showing 35°C.

A few minutes of walking after Acquate we entered the forest and it was so refreshing. From here to the _rifugio_ was very pleasant, even if it was quite a steep ascent, and enjoyable.


## Some pictures

### Wooden-made things

![Rabbit](/assets/2022-07-15/coniglio.jpg)


![Snowman](/assets/2022-07-15/pupazzo_di_neve.jpg)


![Bench](/assets/2022-07-15/panchina.jpg)

### My favorite mountain flower

![My favorite flower](/assets/2022-07-15/fiore.jpg)

### Unstoppable nature

![Plant](/assets/2022-07-15/pianta.jpg)


![Eaten sign](/assets/2022-07-15/indicazione_mangiata.jpg)
