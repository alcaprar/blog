---
layout: post
title:  "Note da un corso di CultureAmp sulle riunioni 1:1"
date:   2022-11-19
tags: notes 1:1 management meeting
---

# Note da un corso di CultureAmp sulle riunioni 1:1

Approccio CAMPS [Certainty (Certezza), Autonomy (Autonomia), Meaning (Significato), Progress (Progresso), Social inclusion (inclusione sociale)).

## Certezza

- Scopi: Come si misura il successo di qualcosa?
- Priorità: Cosa è più importante?
- Ruoli: Cosa fa parte (mansioni, ruoli...) della tua posizione attuale e cosa vuoi e/o puoi aspettarti nel futuro?
- Decisioni: Come vengono decise le cose?

Delle domande che possono aiutare:
- Su una scala da 1 a 10, quanto è chiaro [x y z]?
- Cosa dobbiamo portarci a casa da questa discussione?

## Autonomia

Far capire che hanno possibilità di influenzare, decidere e la loro voce è importante.

- Compito: cosa fanno
- Tempo: quando lo fanno
- Tecnica: come lo fanno
- Squadra: con chi lo fanno


Delle domande che possono aiutare in un progetto:
- Quali pensi che siano i prossimi passi più sensati per il progetto?
- Cosa pensi dovremmo fare ora?
- In che ordine dovremmo fare [x y z]?

Delle domande che possono aiutare in un progetto:
- Ti senti abbastanza responsabile del lavoro che svolgi?
- Dove vorresti avere più/meno autonomia?

## Significato

Se uniamo il lavoro con cose a cui ci tengono poi saranno più coinvolti, soddisfatti e produttivi.

Alcune domande:
- Cosa è importante per te in questo progetto?
- Quali sono gli scopi di questo progetto/lavoro?

## Progresso

Non sono le grandi vittorie ad essere importanti ma un senso di continui risultati e conquiste positive.

Alcuni spunti:
- Cosa hai imparato da questo?
- Dimmi una cosa, anche piccola, che hai fatto bene nella settimana passata.
- Come possiamo dividere questo progetto in parti più piccole?

## Inclusione sociale

Le persone dovrebbe sentire un senso di inclusione. Quando qualcuno si sente escluso, il nostro cervello ci fa soffrire come se fosse un dolore fisico.

- Chiacchere: "Mi dicevi che lo scorso fine settimane avresti fatto [x], come è andata?"
- Essere certi che hanno la possibilità di contribuire: "La prossima volta che abbiamo una riunione con gli altri, potresti condividere quella cosa che hai fatto ...?"
- Qualche controllo veloce sulle dinamiche della squadra: "Come pensi stiano andando le ultime riunioni? Qualcosa che non va? C'è qualcuno che ha dei problemi?"
- Chiedere se vogliono conoscere altra gente nell'azienda fuori dalla squadra in cui lavorano.