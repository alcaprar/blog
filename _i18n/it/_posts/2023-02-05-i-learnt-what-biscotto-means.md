---
layout: post
title:  "Oggi ho scoperto cosa significa biscotto"
date:   2023-02-05
tags: notes random food day-trip
---

# Oggi ho scoperto cosa significa biscotto

Oggi mi sono sentito nello stesso momento un cretino ma anche terribilmente soddisfatto quando ho scoperto il significato di biscotto: bis-cotto -> cotto due volte.

Scoperto casualmente quando cercavo "cibo tipico Novara" durante una gita domenicale improvvisata. E [questa](https://www.thegretaescape.com/blog/prodotti-cucina-tipica-novarese/) è la pagina che mi ha reso una persona più completa.