---
layout: post
title:  "Manager in tempi buii"
date:   2023-02-02
tags: notes management 
---

# Manager in tempi "buii"

L'anno scorso, proprio in questi giorni, mi veniva detto che ero diventato un _Engineering Manager_! Alcuni giorni dopo ho iniziato con una squadra di 3 ingegneri (che fino a qualche giorno fa erano miei colleghi).

Se guardo all'anno passato e provo a confrontarlo con gli ultimi mesi (da Dicembre più o meno) vedo solo una grande differenza: il budget non è più infinito! L'anno scorso sembrava non finire mai: aumenti di stipendio importanti a tutti, promozioni, _offsites_, benefit vari, conferenze, viaggi di lavoro e altro.

È stato un grande aiuto per me che ero un manager alla prima avventura. Non ho dovuto praticamente mai dire di no e sono stato in grado di tenere le persone nella mia squadra grazie a £££ (il mondo dello sviluppo software è stato davvero impazzito [fino ad Aprile quando si è tranquillizato un po'](https://newsletter.pragmaticengineer.com/p/the-scoop-8?s=w). Nella mia azienda questo periodo di gloria è durato fino a fine estate circa.).

Ora le cose sono invece l'opposto. Piccoli aumenti, forse nessuno; le promozioni saranno controllare da un comitato uno ad una; viaggi di lavoro solo su approvazione dei capi dei capi; e altro.

Penso che questo sia la verà realtà e l'anno scorso è stato solamente un periodo assurdo troppo bello per essere vero e duraturo. Godiamoci, e impariamo dal, il vero mondo.