---
layout: post
title:  "Un periodo difficile per Engineering managers"
date:   2024-02-13
tags: notes management
---

Quasi un anno fa ho scritto questa nota ["Manager in tempi buii"](https://al.caprar.xyz/it/2023/02/02/manager-dark-times.html) in cui dicevo che non era facile essere un manager in un periodo in cui il mercato era cambiato da molto positivo (c'erano un sacco di soldi) a molto negativo (bisognava stare attenti agli spicci).

Alcuni mesi dopo son tornato a fare l'ingegnere - _Individual Contributor_ (in realtà lo avevo già deciso 3/4 mesi prima) :).

Oggi ho letto un articolo ([The end of 0% interest rates: what the new normal means for engineering managers and tech leads](https://newsletter.pragmaticengineer.com/p/zirp-engineering-managers)) di Gergely Orosz e ci sono alcune parte che mi son piaciute e mi ci son trovato.

## Essere un manager è più difficile di prima

Questo esprime esattamente quello che provavo quando ho scritto la nota sopra.

```
During the “good times,” of rapid growth, being a manager is rewarding. You hire lots of enthusiastic people, get existing engineers promoted, and when the work gets too much, another new joiner brings fresh energy to the team. Time flies when things are good.

Managing during a downturn is emotionally draining, with little support. When a company is doing poorly, the motivating parts of the job are fewer, like hiring, starting greenfield projects, and promoting high performers. Instead, there’s:

- Planning and executing a layoff handed down from above. We previously covered how to do these humanely
- Learning about layoffs as a line manager at the same time as everyone else
- Trying to restore morale after a downsizing
- Resignations on the team
- Low morale and fear of further cuts
- Having to put a brave face on for team mates affected by decisions you didn’t make
- Personal job worries, as manager positions are at higher risk than ICs

… all during lower pay rises, smaller bonuses, and less compensation than before
```

## Tornare ad essere un Individual contributor

Penso che lo avrei fatto comunque, anche in un perido di calma, ma ora capisco quello che dice nel pezzo seguente:

```
You could go back to being an IC, and act as a hands-on tech lead. This is a path I’m observing many former managers take who used to head up smaller teams. Several had titles like director of engineering or head of engineering, while working with small enough teams.
```

## Ingegneri che non diventano manager può essere positivo

Non voglio dire che sono un buon ingegnere ( :) ) ma che ho sentito la _pressione_ di provare il ruolo da manager. In generale penso di avere le (soft) skills richieste per un manager e per questo mi è stato chiesto quando il manager di prima se ne è andato.

Questo probabilmente non sarebbe successo in periodo come quello di ora e sarei rimasto a fare l'ingegnere.

```
Until recently, there was always pressure at Big Tech and scaleups for standout engineers to become managers. In an environment where teams grow fast, managers kept scouting for candidates to step up to manage a part of the team they didn’t have bandwidth for. Also, a manager with several managers reporting was en-route for a director promotion.

It was easy to get great engineers who were also solid communicators, to take roles. There was no real downside because reverting to an engineer was an option, while being a manager could open new career paths. However, many engineers-turned-managers didn’t go back to engineering, even if they didn’t enjoy the role.

Now, there will be much less pull for engineers to become managers, so some great engineers will probably decide to pass, which is not a bad thing. Engineers with more passion for helping others grow than writing code, will still go for it; and they’re the ones who tend to make great managers.
```