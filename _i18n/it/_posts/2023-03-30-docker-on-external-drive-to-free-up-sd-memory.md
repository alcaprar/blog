---
layout: post
title:  "Spostare Docker su un hard disk esterno per liberare spazio nella SD principale"
date:   2023-03-30
tags: raspberry docker
---

# Spostare Docker su un hard disk esterno per liberare spazio nella SD principale

## Tenativo #1 - fallito

Ho seguito queste guide:
- https://thesmarthomejourney.com/2021/02/11/moving-docker-data-to-an-external-ssd/
- https://www.howtogeek.com/devops/how-to-store-docker-images-and-containers-on-an-external-drive/


Prima di tutto vedere la lista delle periferiche: `df`.

Il mio risultato:
```
File system    1K-blocchi   Usati Disponib. Uso% Montato su
/dev/root         7303220 6419724    529608  93% /
devtmpfs          3834192       0   3834192   0% /dev
tmpfs             3999920     204   3999716   1% /dev/shm
tmpfs             1599968    1228   1598740   1% /run
tmpfs                5120       4      5116   1% /run/lock
/dev/mmcblk0p1     261108   31292    229816  12% /boot
tmpfs              799984     516    799468   1% /run/user/1000
/dev/sda1       960302096      28 911447644   1% /media/alcaprar/lacie
```

Ora creiamo una cartella che verrà usata per montare l'hard disk: `sudo mkdir /mnt/ext-drive`

Poi montiamo l'hard disk sulla nuova cartella: `sudo mount /dev/sda1 /mnt/ext-drive/`. Questo è un montaggio temporaneo.

Rendiamo il montaggio permanente:
1. Bisogna prima trovare lo UUID dell'hard disk: `ls -l /dev/disk/by-uuid`
2. Poi bisogna aggiungere a `/etc/fstab` qualcosa come questo: `UUID=ef63c1cf-6d18-48c3-976d-f8590b8a662d /mnt/ext-drive ext4 defaults 0 0 `

Poi creare (o aggiungere se esiste già) `/etc/docker/daemon.json` e aggiungere:

```
{
    "data-root": "/mnt/ext-drive/docker-data"
}
```

IMPORTANTE: Prima di andare avanti fare un backup!!! Il rischio di perdere dei dati è alto!

Copiare i dati di Docker che erano già salvati nella nuova cartella nell'hard disk: `sudo rsync -aSv /var/lib/docker/ /mnt/ext-drive/docker-data`.

Copiare anche i dati dei volumi: `sudo rsync -aSv ~/self-hosted/data /mnt/ext-drive/self-hosted/data`.

Far ripartire docker deamon: `sudo service docker restart`.

A questo punto, se tutto è andato bene, i servizi dovrebbero ripartire ed avere tutti i dati.

Ora questo è il mio nuovo risultato di `df`:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  5,7G    1,1G  85% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  204K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M  516K    781M   1% /run/user/1000
/dev/sda1       916G  464M    869G   1% /mnt/ext-drive
```

Non sono proprio contento, solo un risparmio dell'8%. Guarderò un po' meglio nei prossimi giorni.


Visto che ho spostato `/var/lib/docker` nell'hard disk esterno lo posso cancellare. Questo però mi ha dato solo l'1% di spazio in più, meglio di niente.

Ora questi sono le 10 cartelli più grandi: `sudo du -aBm / 2>/dev/null | sort -nr | head -n 10`
```
6761M	/
3065M	/usr
1677M	/home/alcaprar
1677M	/home
1605M	/home/alcaprar/.local/share/docker
1605M	/home/alcaprar/.local/share
1605M	/home/alcaprar/.local
1598M	/home/alcaprar/.local/share/docker/overlay2
1524M	/usr/lib
934M	/var
```

`.local/share/docker` occupa la maggior parte quindi forse è ancora in uso.

A questo punto non sono sicuro che quello che ho fatto sopra abbia funzionato. Questo [commento su stackoverflow](https://stackoverflow.com/questions/24309526/how-to-change-the-docker-image-installation-directory#comment116088146_50217666) mi ha aiutato a capire che non ha funzionato. Se faccio `docker info|grep "Docker Root Dir"` ricevo ancora `Docker Root Dir: /home/alcaprar/.local/share/docker` che non ho quello che ho configurato.

## Tentativo #2 - Fallito di nuovo

Ora proverò questa [guida](https://www.baeldung.com/ops/docker-image-change-installation-directory). Questo è quello che ho fatto:
- `docker-compose down`
- backup  della cartella `data` dove faccio il binding dei volumi
- backup di `/mnt/ext-drive/docker-data`, la cartella che pensavo venisse usata da Docker: `sudo rsync -aSv  /mnt/ext-drive/docker-data /mnt/ext-drive/docker-data-backup`. E `sudo rm -r /mnt/ext-drive/docker-data && mkdir /mnt/ext-drive/docker-data`.
- cambiato la riga `ExecStart` nella configurazione del _service_: `sudo vi /lib/systemd/system/docker.service` per essere `ExecStart=/usr/bin/dockerd --data-root /tmp/new-docker-root -H fd:// --containerd=/run/containerd/containerd.sock`
- fatto ripartire docker lanciando `sudo systemctl daemon-reload` e `sudo systemctl restart docker`. Il restart è fallito e lanciando `journalctl -xe` ho visto questo errore `service-start-limit-hit`. Ho trovato da qualche parte online che il `.json` potrebbe interferire e l'ho cancellato. Rilanciando il tutto ha funzionato.

Ma ho riprovato `docker info` senza fortuna. Dice ancora la vecchia cartella.

## Tentativo #3

Ho deciso di ricomnciare da zero con una nuova installazione di Docker.

Prima di tutto ho cancellato tutto: https://askubuntu.com/a/1021506.

Ora ho finalmente un sacco di spazio:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  3,8G    2,9G  58% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  408K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M   32K    782M   1% /run/user/1000
/dev/sda1       916G  2,1G    868G   1% /mnt/ext-drive
```

Ho usato lo [script](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) nella pagina di installazione ufficiale e questa è la situazione spazio dopo l'installazione:
```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  4,2G    2,5G  63% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  408K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
tmpfs           782M   32K    782M   1% /run/user/1000
/dev/sda1       916G  2,1G    868G   1% /mnt/ext-drive
```

E `docker info` ancora mostra quella cartella.


Questa volta voglio provare la strada del collegamento simbolico (_symlink) al posto di giocare con le configurazioni:
1. Stop docker: `sudo systemctl stop docker.socket` and `sudo systemctl stop docker`
2. Visto che è una nuova installazione non c'è niente da spostare/salvare.
3. Fare il collegamento: `ln -s /mnt/ext-drive/docker-data /home/alcaprar/.local/share/docker`
4. Far ripartire docker. Per essere sicuro ho anche fatto un riavvio del Raspberry.
5. Far ripartire il `docker-compose` e aspettare che scaricasse tutte le immagini di nuovo. Tutto funziona!

E, `df -h` ora dice questo:

```
File system     Dim. Usati Dispon. Uso% Montato su
/dev/root       7,0G  4,1G    2,6G  62% /
devtmpfs        3,7G     0    3,7G   0% /dev
tmpfs           3,9G  204K    3,9G   1% /dev/shm
tmpfs           1,6G  1,2M    1,6G   1% /run
tmpfs           5,0M  4,0K    5,0M   1% /run/lock
/dev/mmcblk0p1  255M   31M    225M  12% /boot
/dev/sda1       916G  3,7G    866G   1% /mnt/ext-drive
tmpfs           782M  512K    781M   1% /run/user/1000
```

Finalmente `/` è rimasto uguale e `/dev/sda1` ha preso `1.5 GB`!