---
layout: post
title:  "Monitorare il nuovo Raspberry"
date:   2023-01-21
tags: notes raspberry
---

# Monitorare il nuovo Raspberry

La prima cosa che voglio fare prima che inizio ad installare un po' di programmi vari è avere un buon sistema di monitoraggio. Ho avuto una buona esperienza con [netdata](https://learn.netdata.cloud/) che è pre-installato con CapRover ([ho un'istanza di CapRover in una droplet di DigitalOcean](https://al.caprar.xyz/it/2022/08/20/my-self-hosted-setup.html)) e voglio provare ad usarlo anche per il Raspberry.

L'ho installato seguendo la loro guida ufficiale ["Monitor Pi-hole (and a Raspberry Pi) with Netdata"](https://learn.netdata.cloud/guides/monitor/pi-hole-raspberry-pi): 
- Una riga di comando per installarlo e farlo partire: `wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --stable-channel`.
- Aperto `http://RASP_IP:19999/` e voilà, già funziona!

I sensori sono disabilitati e bisogna attivarli manualmente. Sono curioso di vedere come cambia la temperatura con i vari carichi (per ora è scarichissimo).

1. Aprire la configurazione (apre nano)
```
cd /etc/netdata
sudo ./edit-config charts.d.conf
```
2. Togliere il commento da `sensors=force` e salvare (in nano `ctlx + x` per uscire e poi chiederà conferma per salvarlo.)
3. Far ripartire netdata: `sudo systemctl restart netdata`

E dopo qualche minuto anche i grafici dei sensori hanno iniziato a vedersi!

![Sensors](/assets/2023-01-22/sensors.png)