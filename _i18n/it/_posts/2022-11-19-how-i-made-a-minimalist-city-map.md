---
layout: post
title:  "Come ho fatto un una mappa minimale da incorniciare"
date:   2022-11-19
tags: how-to art maps
---

# Come ho fatto un una mappa minimale da incorniciare

Si vedono spesso sui social o in giro in negozi hipster quelle mappe di città minimali.

Si possono comprare su siti generalisti come [Etsy](https://www.etsy.com/market/minimalist_city_map) o in alcuni più specializzati come [nativemaps.us](https://nativemaps.us/collections/all-maps) o [wijck.com](https://www.wijck.com/en/).

Lo svantaggio di Etsy è che non si possono personalizzare e non tutte le città/aree sono presenti. Questo problema è risolto dagli altri siti più specifici che permettono di scegliere qualsiasi punto da una mappa così da poter fare il poster anche del punto più conosciuto della Terra. Ma questo ha un prezzo 💸.

Mi sono quindi detto: "alla fine è solamente una mappa minimale con un bel quadretto. Ci sarà qualcosa su internet di gratis.". Una ricerca su Google me lo ha confermato: con [snazzymap.com](https://snazzymaps.com)(si può applicare praticamente qualsiasi stile ad una mappa di Google) e [canva.com](https://canva.com)(strumento di design gratuito online che anche non addetti ai lavori possono usare semplicemente) sono riuscito ad ottenere un risultato decente!