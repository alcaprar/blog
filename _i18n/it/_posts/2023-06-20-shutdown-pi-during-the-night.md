---
layout: post
title:  "Spegnere il PI durante la notte"
date:   2023-06-20
tags: raspberry
---

Il Raspberry PI si trova vicino alla mia scrivania e ha un vecchio hard disk collegato. L'hard disk è abbastanza rumoroso e lo puoi sentire chiaramente la mattina e la sera quando c'è silenzio.

Per questo e perchè non lo uso in questi momenti ho pensato che potessi spegnerlo automaticamente durante la notte, tenendolo accesso dalle 9 di mattina alle 9 di sera.

Visto che il Raspberry non si può riaccendere da solo in modo programmatico, userò un timer da giardino che disattiva la corrente fuori da quella finestra temporale (uso tipo quello in foto in questa [risposta di Stackexchange ](https://raspberrypi.stackexchange.com/a/120446)).

L'ultima cosa di cui avevo bisogno è di un cron job che chiami `shutdown` un po' prima che si disattivi l'elettricità giusto per dargli tempo di spegnersi in modo pulito.

Ho lanciato  `sudo crontab -e` (importante: `sudo` perchè deve andare nel crontab di `root` perchè `shutdown` vuole `sudo`) e poi ho aggiunto: `0 21 * * *  /sbin/shutdown -h now`.