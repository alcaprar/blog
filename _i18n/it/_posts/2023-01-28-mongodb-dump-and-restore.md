---
layout: post
title:  "Mongodb - ripristino nel Raspberry"
date:   2023-01-28
tags: notes raspberry
---

# Mongodb - ripristino nel Raspberry

Oggi ho spostato l'ultimo servizio dalla droplet a questo aveva un DB mongo collegato. Per ripristinarlo nel Raspberry ho fatto questo.

1. Dal container `mongodb`, questo comando per copiare il DB: `mongodump -d DB_NAME -o today`. Questo crea una cartella con dentro un po' di `.bjson` files.
2. Comprimere la cartella: `tar -cvf today.tar ./today`
3. File.io ([lo strumento che ho usato quando ho spostato il DB postgres](https://al.caprar.xyz/it/2023/01/25/install-docker-and-deploy-first-service.html)) diceva che non avevo più spazio e ho usato [uguu](https://uguu.se/): `curl -F files[]=@today.tar https://uguu.se/upload.php`
4. Copiare il file nel Raspberry
5. Estrarre il file compresso: `tar -xvf today.tar`
6. Far partire un container `mongodb` con un volume attaccato.
7. Copiare la cartella nel volume
8. Ripristinare il DB: `mongorestore --db DB_NAME FOLDER` (e ho usato `.` come `FOLDER`)