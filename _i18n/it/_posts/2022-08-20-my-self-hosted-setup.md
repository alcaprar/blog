---
layout: post
title:  "Strumenti base per il mio server remoto."
date:   2022-08-20
tags: self-hosted caprover
---

# Strumenti base per il mio server remoto

Un paio di anni fa ho iniziato ad avvicinarmi al mondo del _self-hosted_ e mi è piaciuto sempre di più. Ho provato più strumenti e prodotti prima di arrivare alla configurazione che ho attualmente che mi piace a funziona molto bene.

Per decidere se ero soddisfatto del sistema che avevo "creato" ho usato questi criteri:
- è (facilmente) ripetibile/automatizzato?
- è facile da usare?
- ho abbastanza conoscenze per gestirlo? Se no, riesco ad impararle senza troppi problemi? Se sono troppo complicate o richiedono troppo tempo sicuramente mi stancherò

Detto questo, lo stato attuale è il seguente:
- [DigitalOcean](https://m.do.co/c/9c49b1b21873) come fornitore del server remoto. La _droplet_ più economica che costa ~5/6€ al mese è più che sufficente per il mio utilizzo per ora. Il vantaggio maggiore di [DigitalOcean](https://m.do.co/c/9c49b1b21873) è che è davvero troppo semplice da configurare e non richiede servizi aggiuntivi per avere il _server_ funzionante (_firewall_, _ingress_, _load balancer_... ).
- [CapRover](https://caprover.com/) per gestire i servizi (funziona come un _PaaS_) così da non dovermi occupare di `docker-compose` (`swarm` in realtà mi pare) e `nginx`. Inoltre supporta di suo delle applicazioni [_one-click_](https://github.com/caprover/one-click-apps/tree/master/public/v4/apps) che si possono davvero installare con un solo click. E per finire, si integra [let's encrypt](https://letsencrypt.org/) e in un modo davvero semplice si può avere un certificato per avere tutto sotto HTTPS. Probabilmente scrivero qualcosa di più dettagliato su CapRover perchè mi piace davvero molto.
- dominio comprato su [namecheap.com](namecheap.com) (costa ~10€ all'anno) ma non uso i loro DNS ma quelli di [DigitalOcean](https://m.do.co/c/9c49b1b21873) (ho usato questa [guida](https://www.namecheap.com/support/knowledgebase/article.aspx/10375/2208/how-do-i-link-a-domain-to-my-digitalocean-account/) per collegarli).

Se vuoi puoi replicare questa configurazioni in pochi minuti comprando un dominio super economico e creando una _droplet_ in [DigitalOcean](https://m.do.co/c/9c49b1b21873). Quando crei la _droplet_ puoi scegliere nel m_marketplace_ una che ha già [CapRover installato e configurato](https://marketplace.digitalocean.com/apps/caprover) così nel giro di qualche minuto avrai tutto pronto per rilasciare il primo servizio gestito da te.