---
layout: post
title:  "Posso recuperare i miei backup?"
date:   2023-06-03
tags: raspberry backup
---

Nell'ultimo [post](https://al.caprar.xyz/it/2023/04/09/i-said-i-had-backups.html) parlavo dei miei backup rotti e vuoti.

Qualche giorno fa ero ancora contento di averli risolti e della configurazione del mio Raspberry. Avevo speso un po' di tempo all'inizio su backup e monitoraggio così da dormire sogni più tranquilli più tardi. Poi ho realizzato: "Ho già detto una volta che avevo i backup ma in realtà non era vero. L'ho aggiustato ma non ho mai provato a recuperarli da un altra macchina". E da qui il panico.

Quindi ho deciso di provare a recuperare i backup che vengono salvati in BackBlaze nel mio portatile. Quando ho scritto il post sopra gli avevo recuperati ma direttamente dal Raspberry.

La prima cosa che ho fatto è installare `autorestic` ma per qualche ragione simile a quelle scritte [qui](https://al.caprar.xyz/it/2023/01/24/backup-raspberry.html) non ci son riuscito. Non avevo proprio voglia di combatterci di nuovo e mi son ricordato che offrissero anche una [Docker image](https://autorestic.vercel.app/docker) e l'ho usata.

A questo punto mi sono accorto che la paura iniziale era vera: non avevo salvato la _key_ e il file `autorestic.yaml` ma solamente le chiavi di accesso a BackBlaze. Ma come è scritto chiaramente nella loro [documentazione](https://autorestic.vercel.app/quick), senza la _key_ i dati sono inutili perchè vengono criptati nel server:

![warning about data loss](/assets/2023-06-03/warning.png)

Quindi la prima cosa che ho fatto a questo punto è `ssh` al Raspberry e ho salvato il file `autorestic.yml` nel mio [Bitwarden](https://al.caprar.xyz/en/2023/02/19/more-privacy-reflection.html#password-manager).

Poi ho creato un file `autorestic.yml` sul mio portatile e ho eseguito:

```bash
docker run --rm -v $(pwd):/data cupcakearmy/autorestic autorestic restore -l home -c /data/autorestic.yml --to /data/restore
```

Ha funzionato correttamente e mi son trovato la cartella `restore`. Ho provato a fare `cd` ma mi dava permesso negato. Cazzarola. Ho provato con `ls -s` e ho notato che tutto era proprietà di `root` così ho eseguito (grazie [Stackoverflow](https://askubuntu.com/a/720000) che mi posso continuare a dimenticare comandi base): `sudo chmod a+rwx restore`. Riprovato di nuovo a fare `cd` e avevo tutti i miei file al sicuro.