---
layout: post
title:  "Accedere al Raspberry dall'esterno senza aprire le porte"
date:   2023-01-23
tags: notes raspberry
---

# Accedere al Raspberry dall'esterno senza aprire le porte

Avevo già sentito parlare di [Cloudflare](https://www.cloudflare.com/it-it/products/tunnel/) ma prima di usarlo volevo esplorare anche altre alternative. Questo è quello che ho trovato:
- [loophole](https://loophole.cloud/)
- [zerotier](https://www.zerotier.com/) (da questa [discussione reddit](https://www.reddit.com/r/selfhosted/comments/s15scj/zerotier_vs_cloudflare_tunnelngrokalternatives/))
- [lista di servizi per fare tunneling](https://github.com/anderspitman/awesome-tunneling)

Alla fine ho deciso di andare per la soluzione più facile e stabile, anche se il codice è chiuso: Cloudflare tunnel. Penso (e spero) che sarà davvero facile installaro e gestirlo. Gli darò un po' di tempo, qualche settimana/mese, e poi valuterò.

Ho provato a giocare un po' con la dashboard ma non ho ben capito come fare per il tunnel e alla fine ho trovato [guida](https://pimylifeup.com/raspberry-pi-cloudflare-tunnel/) ([pagina archiviata](https://web.archive.org/web/20221216160856/https://pimylifeup.com/raspberry-pi-cloudflare-tunnel/)).

Però prima di iniziare con il terminale ho dovuto fare queste due cose:
1. Aggiungere un sito a Cloudflare
2. Cambiare i nameservers del dominio per farli puntare a Cloudflare

Il primo punto è stato immediato, mi è bastato seguire i vari passi. Il secondo è stato pure semplice ma richiederà fino a 24 ore per aggiornarsi.

Quando i nameservers si sono aggiornati ho dovuto sistemare i DNS e poi ho seguito la guida. Tutti i passi hanno funzionato correttamente, tranne l'ultimo: quando aprivo l'URL collegato al tunnel mi ritornava uno strano error. Come se non esistesse la pagina :(.

Ho cambiato un po' di impostazioni dal pannello di controllo di Cloudflare e alla fine son riuscito a sistemarlo. Non sono certo ma penso che sia bastato rimuovere `proxied` a `DNS only` nel dominio principale.

Ora il servizio è raggiungibile dall'esterno ma a tutti. Voglia metterlo dietro un'autenticazione con Zero trust. È stato facilissimo: basta aggiungere una nuova applicazione, create una nuova policy e collegarla al dominio.