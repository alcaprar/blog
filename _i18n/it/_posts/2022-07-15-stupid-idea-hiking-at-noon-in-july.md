---
layout: post
title:  "Fare un trekking a mezzogiorno a Luglio non è una buona idea"
date:   2022-07-15
tags: hiking trekking lago-como lago-lecco resegone
---

Qualche giorno fa sono andato con mio fratello per un trekking di 2 giorni al [monte Resegone](https://it.wikipedia.org/wiki/Resegone), vicino a Lecco. Abbiamo prenotato una notte al [Rifugio Stoppani](https://www.rifugiostoppani.it/) in una camerata condivisa e abbiamo deciso di andare in treno e a piedi.

Siam partiti da Milano Centrale con un treno Trenord fino a Lecco. Dalla stazione abbiamo camminato fino ad [Acquate](https://goo.gl/maps/7C9WWcGv3JPtdJNK7) e poi abbiamo seguito la [guida](https://www.rifugiostoppani.it/percorso-da-acquate.html) che ci aveva indicato il proprietario del rifugio. Acquate è una frazione di Lecco ed è famosa per essere stata l'ambientazione di molte scene dei Promessi Sposi e la città natale di Renzo e Lucia.

La prima parte, dalla stazione fino ad Acquate è stata terribile: pochissima ombra e troppe strade e palazzi che rilasciavano una quantità di calore allucinante. Ed era tutto in salita. Siamo andati pianissimo e ad ogni passo ci maledicevamo per essere partiti a mezzogiorno. Il termometro segnava 35°C.

Appena arrivati ad Acquate, dopo poche centinaia di metri siamo entrati nel bosco ed è stata una liberazione. Da qui al rifugio è stata una passeggiata rilassante e _fresca_.


## Foto a caso

### Cose fatte di legno

![Coniglio](/assets/2022-07-15/coniglio.jpg)


![Pupazzo di neve](/assets/2022-07-15/pupazzo_di_neve.jpg)


![Panchina](/assets/2022-07-15/panchina.jpg)

### Il mio fiore di montagna preferito

![Il mio fiore preferito](/assets/2022-07-15/fiore.jpg)

### La natura che si riprende tutto

![Pianta](/assets/2022-07-15/pianta.jpg)


![Indicazione mangiata](/assets/2022-07-15/indicazione_mangiata.jpg)
