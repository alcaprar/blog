---
layout: post
title:  "Raspberry - primo giorno"
date:   2023-01-21
tags: notes raspberry
---

# Raspberry - primo giorno

Oggi ho aperto il pacco con il Raspberry PI 4 che ho ricevuto a Natale (sì, ancora qualcuno mi fa i regali e ci prendono :D). Ne avevo uno quando ero all'università e ci ho giocato un po' ma non troppo. Infatti non mi ricordo neanche per cosa lo usavo (mi ricordo solamente di un piccolo esperimento con OpenCV e una webcam wifi per controllare chi entrava in casa).

Sono stato davvero sorpreso e contento dalla facilità di installazione di Raspbian. Sono partito dalla [pagina ufficiale](https://www.raspberrypi.com/software/),  poi ho scaricato Raspberry PI imager (`sudo apt install rpi-imager`) e seguito le istruzioni per preparare la SD (sto usando una da 8 GB ma solo per fare qualche test. Poi ne cercherò una più grande).

L'installazione del sistema operativo nella SD ci ha messo 5-10 poi l'ho messa nel Raspberry, attaccato un mouse, una tastiera e la TV. Appena acceso ha fatto delle cose per la configurazione iniziale e in pochi minuti (beh in realtà l'installazione degli aggiornamenti ci ha messo un po') ero dentro!

Per oggi ho solamente attivato l'accesso SSH (seguendo questa [guida](https://www.howtogeek.com/768053/how-to-ssh-into-your-raspberry-pi/)) così potro usarlo da remoto senza TV, mouse e tastiera.