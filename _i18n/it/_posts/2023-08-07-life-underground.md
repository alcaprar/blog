---
layout: post
title:  "Vita sottoterra"
date:   2023-08-07
tags: life animals
---

Oggi ho fatto alcuni lavoretti in giardino e ho spostato alcune mattonelle in cotto per togliere delle erbacce.

È stato molto interessante vedere quanta vita esiste sottoterra.

## Formiche

Questa è una casa disabitata. Nessuno si è fatto vedere.

![Vecchia casa di formiche](/assets/2023-08-07/old_ants_house.JPG)

Questa invece è molto affollata.

[Tante formiche](/assets/2023-08-07/ants.mp4)

 <video width="320" height="240" controls>
  <source src="/assets/2023-08-07/ants.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## 2 lucertole future

![Uova di lucertola](/assets/2023-08-07/lizard_eggs.JPG)

## Lumaca nuda

[Lumaca nuda](/assets/2023-08-07/snail.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/snail.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## Run away

[Scappa 1](/assets/2023-08-07/run-away1.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/run-away1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

[Scappa 2](/assets/2023-08-07/run-away2.mp4)

<video width="320" height="240" controls>
  <source src="/assets/2023-08-07/run-away2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 