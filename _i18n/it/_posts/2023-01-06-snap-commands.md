---
layout: post
title:  "Comandi Snap"
date:   2023-01-06
tags: notes linux terminal
---

# Comandi Snap

- Installare un pacchetto: `snap install <PACKAGE_NAME>`
- Recuperare informazioni su un pacchetto installato `snap info <PACKAGE_NAME>`
- Aggiornare un pacchetto: `snap refresh <PACKAGE_NAME>`
- Aggiornare tutti: `snap refresh`
- Rimuovere un pacchetto: `snap remove <PACKAGE_NAME>`