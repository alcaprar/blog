---
layout: post
title:  "Incontrato una Salamandra"
date:   2023-02-26
tags: hiking trekking lago-lugano salamandra animali
---

# Incontrato una Salamandra

Ieri sono andato a fare un trekking sulle montagne intorno al lago di Lugano. Insieme ad un'amico siamo partiti da Milano Porta Garibaldi in treno e in 1 ora e 20 min siamo arrivati a Porto Ceresio.

Da qui abbiamo seguito questa [traccia]((https://it.wikiloc.com/percorsi-escursionismo/monte-san-giorgio-dalla-stazione-di-porto-ceresio-97183837)) presa da Wikiloc (qui il [KML](/assets/2023-02-26/monte-san-giorgio-dalla-stazione-di-porto-ceresio.kml) che ho usato per seguire la traccia senza Internet con [OrganicMaps](https://al.caprar.xyz/it/2023/02/19/more-privacy-reflection.html#mappe)).

La vista è stupenda per quasi tutto il giro e si cammina spesso sotto un rinfrescante bosco.

E abbiamo incontrato una Salamandra!

![Salamandra](/assets/2023-02-26/salamandra.jpg)

![Bosco](/assets/2023-02-26/bosco.jpg)

![Vista](/assets/2023-02-26/vista.jpg)