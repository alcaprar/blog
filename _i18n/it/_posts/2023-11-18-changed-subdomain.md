---
layout: post
title:  "Ho cambiato il dominio di questo sito"
date:   2023-11-18
tags: cloudflare google domain
---

Ho deciso di cambiare il dominio di questo sito, in particolare di rimuovere la parola `blog` perchè alla fine è una semplice collezione di note a caso.

È stato abbastanza facile:
1. ho cambiato le configurazione in Jekyll
2. ho rinominato tutte le occorrenze in questo repo così i vecchi link punteranno al nuovo dominio
3. ho aggiunto il nuovo dominio nelle configurazioni di GitLab pages
  3.1 ho verificato il nuovo dominio aggiungendo una riga TXT nei DNS di Cloudflare
4. ho aggiunto un nuova riga CNAME nei DNS di Cloudflare puntando a GitLab
5. ho aggiunto un _bulk redirect_ in Cloudflare così se qualcuno arriva nel vecchio dominio verrà mandato sul nuovo (ho seguito [questa guida](https://developers.cloudflare.com/fundamentals/setup/manage-domains/redirect-domain/) - [archiviata](https://web.archive.org/web/20231024051634/https://developers.cloudflare.com/fundamentals/setup/manage-domains/redirect-domain/)). Ho abilitato tutti i parametri che uscivano nella pagina: `Preserve query string`, `Include subdomains`, `Subpath matching`, `Preserve path suffix`.