---
layout: post
title:  "Installare Docker e rilasciare la prima applicazione"
date:   2023-01-25
tags: notes raspberry
---

# Installare Docker e rilasciare la prima applicazione

Per installare Docker ho seguito questa [guida](https://phoenixnap.com/kb/docker-on-raspberry-pi) ([pagina archiviata](https://web.archive.org/web/20221027153913/https://phoenixnap.com/kb/docker-on-raspberry-pi)).

Ci ha messo un po' di tempo (5-10 minuti) a la CPU al massimo è andata al 50% (controllando netdata).

Quando l'installazione è finita ho creato l'utente non-root ma, al posto di fare come spiegato nell'articolo, ho lanciato `dockerd-rootless-setuptool.sh install` come suggerito dall'output dell'installazione (la prima volta ha fallito ma ho semplicemente dovuto installare dei pacchetti mancanti). Poi ho semplicemente aggiunto `export DOCKER_HOST=unix:///run/user/1000/docker.sock` al `.bashrc`.

Per controllare che tutto funzionasse ho eseguito: `docker info` e `docker run hello-world`. Se esce questo dovrebbe funzionare:
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
7050e35b49f5: Pull complete 
Digest: sha256:aa0cc8055b82dc2509bed2e19b275c8f463506616377219d9642221ab53cf9fe
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
```

Ora voglio installare anche `docker-compose` e ho lanciato questi comandi (presi da questo [articolo](https://devdojo.com/bobbyiliev/how-to-install-docker-and-docker-compose-on-raspberry-pi)):
```
sudo apt-get install libffi-dev libssl-dev
sudo apt install python3-dev
sudo apt-get install -y python3 python3-pip

sudo pip3 install docker-compose
```

E ora è davvero tutto pronto!

Però prima di installare qualche applicazione voglio attivare il monitoraggio di `Docker` da parte di `netdata`. Visto che netdata era in funzione già da prima dell'installazione di `Docker`, ho dovuto far ripartire il servizio: `sudo service netdata restart`.

Ora è davvero tutto pronto per installare il mio RSS reader, [miniflux](https://miniflux.app/). È uno strumento fantastico che per il momento avevo su una droplet di DigitalOcean. Voglio fare un dump del DB e far partire questa nuova istanza con gli stessi contenuti.

Come prima cosa ho provato a fare il dump del DB usand uno strumento con interfaccia grafica perchè avevo dei problemi a fare SSH alla droplet. Ho provato `pgadmin` e `pgweb` ma falliti entrambi.

Alla fine son riuscito seguento questa strategia (usando il terminale nel browser che si apre dal pannello di DigitalOcean):
- far partire un terminale nel container di postgres: `docker exec -it DOCKER_ID bash`
- lanciare il comando per fare il dump: `pg_dump  -U miniflux -d miniflux --column-inserts  > 2023-01-25.sql`
- spostare il file nella cartella che è attaccata al volume Docker: `mv 2023-01-25.sql /var/lib/postgres/data`
- tornare all'host (droplet) e entrare nella cartella dove Docker salva i volumi: `cd /var/lib/docker/volumes`
- `ls` per vedere quali cartelle ci sono e capire qual è quella giusta. Fare `cd` in quella.
- visto che non riuscivo a fare SSH nella droplet non potevo neanche salvare i file nel mio computer. Per farlo ho usato un servizio terzo: `curl -F file=@2023-01-25.sql https://file.io`
- nel passo precendente viene restituito un URL con la destinazione del file. Aprendo dal browser l'ho scaricato. Dopo lo scaricamento dovrebbe essere cancellato dai loro server (lo spero :)).

Ora ho un dump del DB nel mio computer, devo solamente caricarlo nel Raspberry e fare il ripristino:
- copiare il file nel Raspberry: `scp 2023-01-24.sql alcaprar@192.168.1.3:/home/alcaprar`
- far partire un container docker con volume persistente (docker-compose.yml più avanti)
- spostare il file nella cartella usata per il volume
- iniziare un terminale nel container postgres
- creare un nuovo database: `psql -U postgres`. Then `create database miniflux;` and exit `\q`
- ripristinare il DB `psql -U postgres -d miniflux < 2023-01-25.sql`

A questo punto il DB dovrebbe essere ripristinato e dovrebbe essere sufficiente far partire anche miniflux. Questo è `docker-compose` corrente:
```
version: '3.4'
services:
  miniflux:
    image: miniflux/miniflux:2.0.36
    ports:
      - "8080:8080"
    depends_on:
      - db
    environment:
      - DATABASE_URL=postgres://postgres:postgres@db/miniflux?sslmode=disable
  db:
    image: postgres:15
    environment:
      - POSTGRES_PASSWORD=postgres
    volumes:
      - ./data/postgres:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "postgres"]
      interval: 10s
      start_period: 30s
```

Dopo un `docker-compose up -d` miniflux dovrebbe essere partire e si può accedere dalla porta 8080. Tutti i contenuti dovrebbero essere li.

A questo punto voglio poter accedere dall'esterno usando [Cloudflare tunnels come ho fatto per netdata](https://al.caprar.xyz/it/2023/01/23/accessing-raspberry-from-outside-without-opening-ports.html).

Per aggiungere un nuovo servizio a Cloudflare tunnels:
- spegnere il servizio cloudflared: `sudo service cloudflared stop`
- disinstallare il servizio: `sudo cloudflared service uninstall`
- rimuovere il file di configurazione da `/etc/cloudflared/config.yml`: `sudo rm /etc/cloudflared/config.yml`
- aggiungere il nuovo servizio a `.cloudflared/config.yml`
- aggiungere la nuova regola al dns: `cloudflared tunnel route dns pi4 rss.caprar.xyz`
- reinstallare il servizio: `sudo cloudflared --config ~/.cloudflared/config.yml service install`