---
layout: post
title:  "Backup Raspberry"
date:   2023-01-24
tags: notes raspberry backup
---

# Backup Raspberry

Questo dovrebbe essere l'ultimo passo della configurazione iniziale del Raspberry che volevo. Dopo di questo posso cominciare a giocarchi e aggiungere nuovi servizi più tranquillamente. Le altre due cose che ho fatto sono:
- [installato netdata per poter monitorarlo](https://al.caprar.xyz/it/2023/01/21/monitoring-raspberry.html)
- [impostato CloudFlare tunnel per poter accederlo dall'esterno della mia rete senza aprire le porte del router](https://al.caprar.xyz/it/2023/01/23/accessing-raspberry-from-outside-without-opening-ports.html)

Lo strumento per i backup che sto considerando è `autorestic`. Non l'ho mai usato prima di ora ma ho avuto delle buone recensioni da colleghi e amici e ho trovato dei bei articoli online.

La destinazione del backup per ora sarà [backbaze B2](https://backblaze.com). Anche in questo caso non l'ho mai usato, ma ne ho sentito parlare bene e sembra essere molto economico.

Ho iniziato seguendo la loro [guida](https://autorestic.vercel.app/installation) per l'installazione che ha un comando di una riga per l'installazione.

Ho provato ad lanciarlo (`wget -qO - https://raw.githubusercontent.com/cupcakearmy/autorestic/master/install.sh | bash`) e ho ricevuto:

```
linux
arm64
/usr/local/bin/autorestic.bz2: Permesso negato
bzip2: Can't open input file /usr/local/bin/autorestic.bz2: No such file or directory.
chmod: impossibile accedere a '/usr/local/bin/autorestic': File o directory non esistente
bash: riga 47: autorestic: comando non trovato
Successfully installed autorestic
```

Non riuscivo a capire cosa stesse succedendo e ho provato a replicare a mano quello che [install.sh](https://github.com/cupcakearmy/autorestic/blob/c82db56069dcf519a680d24b635263d6b6e8bc3f/install.sh) dovrebbe fare:
```
wget https://github.com/cupcakearmy/autorestic/releases/download/v1.7.5/autorestic_1.7.5_linux_arm64.bz2
bzip2 -fd autorestic_1.7.5_linux_arm64.bz2
./autorestic_1.7.5_linux_arm64 install
```

L'ultimo comando falliva:
```
Downloading: https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
os.Rename() failed (rename /tmp/autorestic-3881862718 /usr/local/bin/restic: permission denied), retrying with io.Copy()
Error: open /usr/local/bin/restic: permission denied
cannot lock before reading config location
```

e l'ho lanciato con `sudo`:
```
sudo ./autorestic_1.7.5_linux_arm64 install
Downloading: https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
Successfully installed 'restic' under /usr/local/bin
```

Pensavo che fosse tutto installato ma mi sembra che abbia installato solo `restic`. Dopo aver controllato meglio il file `install.sh` ho notato di aver mancato un passaggio: spostare il binario `autorestic` in `/usr/local/bin`: `sudo mv autorestic_1.7.5_linux_arm64 /usr/local/bin/autorestic`.

In questo momento penso di avere tutto installato e, dopo aver creato `.autorestic.yml`, lancio: `autorestic check -v`

```
Using config paths: . /home/alcaprar autorestic
Using config: 	 /home/alcaprar/autorestic/.autorestic.yml
Using lock:	 /home/alcaprar/autorestic/.autorestic.lock.yml
> Executing: /usr/local/bin/restic check
Initializing backend "backblaze"...
> Executing: /usr/local/bin/restic init
Error: exit status 1
```

Dopo aver riprovato varie volte l'unica cosa che mi veniva in mente era di aver fatto dei casini con `sudo` e quindi ho provato ad installarlo in `/home/USER/bin` invece di `/usr/local/bin`. From your home:
```
wget https://github.com/cupcakearmy/autorestic/releases/download/v1.7.5/autorestic_1.7.5_linux_arm64.bz2
bzip2 -fd autorestic_1.7.5_linux_arm64.bz2
chmod +x autorestic_1.7.5_linux_arm64.bz2
mv autorestic_1.7.5_linux_arm64.bz2 ./bin/autorestic
wget https://github.com/restic/restic/releases/download/v0.15.0/restic_0.15.0_linux_arm64.bz2
bzip2 -fd restic_0.15.0_linux_arm64.bz2
chmod +x restic_0.15.0_linux_arm64.bz2
mv restic_0.15.0_linux_arm64.bz2 ./bin/restic
```

A questo punto dovrei aver tutto di nuovo installato e ho riprovato `autorestic check -v` ma senza fortuna :(. Cercando un po' su google e nel repo github ho trovato questo [commento in un issue](https://github.com/cupcakearmy/autorestic/issues/147#issuecomment-1135094548) che dice che il comando di `backup` di `autorestic` restituisce anche l'output di `restic` sottostante mentre `check` no. L'ho provato (`autorestic backup -a -v`), e infatti restic mi ha detto che avevo un errore nel file di configurazione. 

Sistemato in due secondi e riprovato subito `autorestic check -v` che ha funzionato alla grande. Poi subito `autorestic backup -a`. Funziona!!

Per oggi è tutto. È stato davvero abbastanza semplice (a parte quel problem con il config che mi ha mandato su una strada sbagliata). La prossima cosa è renderlo ricorrente.