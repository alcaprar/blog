---
layout: post
title:  "Come sto provando ad usare linkding come un alternativa privata a Good Reads"
date:   2022-11-12
tags: self-hosted linkding good-reads
---

# Come sto provando ad usare linkding come un alternativa privata a Good Reads

Un paio di settimane fa mi sono imbattuto casualmente in questo [articolo](https://cri.dev/posts/2021-04-05-currently-self-hosting-apps/) di [cri.dev](https://cri.dev) dove lui parla di quali strumenti gira nel suo server. 

Uno nella lista ha colpito la mia attenzione: [linkding](https://github.com/sissbruecker/linkding). Non ne avevo mai sentito parlare prima e ho dovuto cercare su Google cosa fosse. La descrizione nel repo GitHub dice: `linkding è un servizio per salvare i preferiti semplice che puoi usare nei tuoi server. È pensato per essere minimale, veloce e facile da gestire con Docker` (originale: `linkding is a simple bookmark service that you can host yourself. It's designed be to be minimal, fast, and easy to set up using Docker`).

Mi ha subito intrigato perchè era proprio qualcosa che mi mancava. Prima salvavo i miei preferiti, siti, collezioni e libri (letti e da leggere) in una tabella su [nocodb](https://www.nocodb.com/) che avevo nel mio server. Funzionava bene ma era un po' scomodo (non aveva app e la versione web non si adattava bene al telefono) quindi l'idea di provare linkding mi è subito piaciuta.

L'ho installato con un paio di click nel mio CapRover (se vuoi sapere di più come gestisco il mio server leggi [Strumenti base per il mio server remoto](https://al.caprar.xyz/it/2022/08/20/my-self-hosted-setup.html)) e ho subito capito che era quello che mi mancava. Con un po' di lavoro manuale ho spostato tutto quello che avevo da nocodb a linkding. Ho sfruttato questo momento anche per fare una pulizia e togliere le cose che non erano più rilevanti.

La parte che per ora mi piace di più è come ho strutturato il catalogo, principalmente per i libri ma funziona bene anche per video, film, articoli..: per ogni libro aggiungo un nuovo preferito a linkding:
- l'`URL` è una qualsiasi pagina internet che lo descrive (pagina di Wikipedia, pagine di Good reads...);
- il `title` è il titolo del libro;
- la `description` contiene le mie note/recensioni che ogni tanto scrivo per i libri che ho letto.

Poi ho iniziato ad usare i `tags` per organizzarli:
- ogni libro ha il tag `libri`
- ogni libro può avere uno tra questi: `da-iniziare`, `in-corso`, `finito`.

Con questo modo posso facilmente filtrare i libri e trovare le note di quelli che ho già letto (`#libri finito`) oppure avere la lista di quelli che ho salvato per il futuro (`#libri #da-iniziare`).