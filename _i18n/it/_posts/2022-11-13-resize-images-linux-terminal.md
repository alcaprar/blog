---
layout: post
title:  "Ridimensionare immagini da terminale Linux"
date:   2022-11-13
tags: notes linux terminal
---

# Ridimensionare immagini da terminale Linux

Installare il programma:
- `sudo apt get install imagemagick`

Alcuni comandi utili:
- `convert example.jpg -resize 1000x800 example.jpg`: mantiene il rapporto iniziale. L'immagine verrà ridimensionata per stare nell'area. Il file sarà sovrascritto.
- `convert example.jpg -resize 1000 example.jpg`: mantiene il rapporto iniziale. L'immagine verrà ridimensionata per avere al massimo 1000 pixel di larghezza. Il file sarà sovrascritto.
- `convert example.jpg -resize x800 example.jpg`: mantiene il rapporto iniziale. L'immagine verrà ridimensionata per avere al massimo 800 pixel di altezza. Il file sarà sovrascritto.
- `convert example.jpg -resize 1000x800! example_new.jpg`: ridimensiona l'immagine alla dimensione passata (anche se rovina le proporzioni). Un nuovo file verrà creato e l'originale non verrà toccato.

[Sorgente](https://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/)([pagina archiviata](https://web.archive.org/web/20221027192723/https://www.howtogeek.com/109369/how-to-quickly-resize-convert-modify-images-from-the-linux-terminal/)).