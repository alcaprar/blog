---
layout: post
title:  "Avevo detto di avere i backup"
date:   2023-04-09
tags: raspberry backup
---

# Avevo detto di avere i backup

Quando ho iniziato a configurare il mio raspberry avevo detto che una delle prime cose che volevo avere prima di iniziare ad installarci altro erano i backup. E infatti era proprio quello che pensavo di aver fatto e ho scritto [qui](https://al.caprar.xyz/it/2023/01/24/backup-raspberry.html).

Pensavo funzionasse bene, qualcosa veniva caricato sul bucket ed ero contento.

Un paio di settimane fa ho [spostato la cartella di sistema di docker su un hard-disk esterno](https://al.caprar.xyz/en/2023/03/30/docker-on-external-drive-to-free-up-sd-memory.html)  per ridurre il carico sulla SD card e devo aver fatto qualche stronzata mentre lo facevo. Ho avuto bisogno di ripristinare il DB di `miniflux` (fortunatamente l'unico servizio a cui si è corrotto il DB). Avendo dei backup giornalieri pensavo che avrei perso solo pochi dati.

Dopo aver lanciato `autorestic restore` e aperto `miniflux.sql` ho trovato: `error while making the dump. Container not found.`. La mia reazione: "ma che cazzo!".

Questa esperienza mi ha ricordato che prima di dire che ho un sistema di backup che funziona avrei dovuto testare anche la parte di ripristino.

Comunque, ora funziona. Ho aggiustato lo script del backup e testato il ripristino. :)