---
layout: post
title:  "Sacro Monte di Varese"
date:   2023-07-16
tags: hiking trekking lago-varese animals
---

Ieri siamo andati a fare un bel giro nei dintorni di Varese, dal [Sacro Monte di Varese](http://www.sacromontedivarese.it/) fino a Galvirate passando per il Forte di Orino.

Abbiamo preso il treno da Milano per Varese, poi un autobus locale (il `C` mi pare) fino alla [prima Cappella](https://osm.org/go/0CklE0GYA?m=).

Da qui abbiamo seguito un mix di questi 3 itinerari trovati su Wikiloc:
- [Sacro Monte di Varese - Monte tre croci - Osservatorio](https://it.wikiloc.com/percorsi-escursionismo/sacro-monte-di-varese-monte-tre-croci-osservatorio-45970230)([KML](/assets/2023-07-16/sacro-monte-di-varese-monte-tre-croci-osservatorio.kml))
- [Da pensione Irma a Forte di Orino](https://it.wikiloc.com/percorsi-escursionismo/da-pensione-irma-a-forte-di-orino-62394231)([KML](/assets/2023-07-16/da-pensione-irma-a-forte-di-orino.kml))
- [Gavirate - Forte di Orino Sentiero 13 /313](https://it.wikiloc.com/percorsi-escursionismo/gavirate-forte-di-orino-sentiero-13-313-92246172)([KML](/assets/2023-07-16/gavirate-forte-di-orino-sentiero-13-313.kml))

## Animali

![Piccolo animale #1](/assets/2023-07-16/DSC_0918.JPG)

![Piccolo animale #2](/assets/2023-07-16/DSC_0920.JPG)

![Piccolo animale #3](/assets/2023-07-16/DSC_0923.JPG)

![Piccolo animale #4](/assets/2023-07-16/DSC_0927.JPG)

## Alberi abbattuti dalla tempesta Alex

Mentre passeggiavamo ci siamo imbatutti nella devastazione fatta dalla [tempesta Alex](https://www.varesenews.it/2023/02/due-ottobre-2020-la-tempesta-alex-devasta-le-valli-del-varesotto-serata-analizzare-cose-accaduto/1548978/) circa 3 anni fa.

![Alberi abbattuti dalla tempesta Alex](/assets/2023-07-16/DSC_0926.JPG)