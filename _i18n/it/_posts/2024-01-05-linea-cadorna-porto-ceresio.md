---
layout: post
title:  "Linea Cadorna - Lago di Lugano"
date:   2024-01-05
tags: hiking trekking lago lugano
---

Oggi io e mio fratello siamo andati a fare un giro sul lago di Lugano per vedere i resti (intatti) delle trincee della seconda guerra mondiale che erano state costruite per difendere questo lato dell'Italia e mai usate.

Abbiamo preso un treno da Milano fino a Porto Ceresio (1 ora e 15 minuti) e da li siamo saliti su per il Monte Casolo e poi il Monte Pravello. Da qui poi è tutta discesa e fino al Monte Orsa si passa in mezzo alle trince. Ci sono anche 2 grandi cannoni dentro a delle gallerie.

[Traccia GPX](/assets/2024-01-05/linea-cadorna.gpx).

Alcune foto:

![Tunnel](/assets/2024-01-05/tunnel.jpg)
![Cannoni](/assets/2024-01-05/cannon.jpg)
![Trincee](/assets/2024-01-05/trench.jpg)
![Vista](/assets/2024-01-05/view.jpg)