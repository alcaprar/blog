---
layout: post
title:  "Più privacy, una riflessione"
date:   2023-02-19
tags: notes privacy
---

# Più privacy, una riflessione

Un paio di anni fa', forse più, ho cominciato ad interessarmi di più della mia privacy online. Prima di questo punto di svolta non mi impegnavo tanto e in realtà quasi mi piacevano tutte quelle raccomandazione dei vari siti così precise e mirate. Mi ero anche registrato praticamente ovunque e spesso i contenuti che pubblicavo erano pubblici. E, usavo tantissimo Google.

Ad un certo punto ho deciso che era ora di cambiare e con questo articolo provo a fare un riepilogo di quello che ho fatto finora.

Ho ancora molto da fare visto che purtroppo ancora uso troppo i servizi di Google. Più avanti proverò a pensare cosa posso ancora fare e scriverò due righe.

## Browser

Ero un grande sostenitore di Google Chrome perchè è molto integrato con tutti i servizi di Google.

Sul mio portatile ora uso Firefox con l'estensione [_multi-containers_](https://github.com/mozilla/multi-account-containers#readme). Provo a segregare tutto (un _container_ per ogni sito in cui sono registrato con l'opzione "visita questo sito in questo _container_") e per le ricerce estemporanee uso sempre un _container_ temporaneo.

Sul telefono:
- [Firefox focus](https://www.mozilla.org/it/firefox/browsers/mobile/focus/) per praticamente tutte le necessità
- Firefox per quei siti in cui è comodo rimanere _loggati_

## Motore di ricerca

Ho sempre usato Google (come l'84% degli utenti nel mondo, dato di [Dicembre 2022](https://www.statista.com/statistics/216573/worldwide-market-share-of-search-engines/)) come unico motore di ricerca. Non pensavo esistesse altro.

Ora uso [Duckduckgo](https://duckduckgo.com/) per il 95% delle mie ricerche. In alcuni casi devo purtroppo usare Google perchè è dannatamente forte come motore di ricerca.

## Gestore delle password

Come i punti precedenti, usavo il servizio di gestore delle password di Google che si integra molto bene tra Google Chrome e un telefono Android.

Ora sono un felice cliente pagante di [Bitwarden](https://bitwarden.com/) e lo condivido con la mia famiglia (il piano famiglia è fino a 5 persone).

Devo dire che è stato più facile del previsto. Sul computer uso l'estensione di Firefox e sul telefono uso la loro app. Funzionano molto bene e l'esperienza è la stessa del gestore delle password di Google.

## Notizie

Non usavo un solo servizio in particolare ma un misto di: Google news, Feedly, pagine sui social media di giornali, siti di quotidiani...

Ho smesso di usare direttamente i siti e i vari servizi. Ora uso [Miniflux](https://miniflux.app/), un lettore RSS, e lo faccio girare sul mio Raspberry. Non riesco proprio a pensare un mondo senza di questo strumento.

## Tastiera Android

Usavo la [Gboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin&gl=US), la tastiera di Google. Come puoi immaginare, questa manda un casino di info a Google ed è effettivamente uno degli strumenti migliore per Google per migliorare il nostro profilo.

Ora uso [OpenBoard](https://github.com/openboard-team/openboard) e sono abbastanza felice. Supporta la possibilità di crearsi dei dizionari personali ed è molto facile cambiare tra più lingue.

Un punto negativo, ma ormai mi sono abituato senza, è la mancanza della digitazione scorrendo (_swipe-to-type_). Prima di iniziare ad usare questa nuova tastiera la usavo spesso e mi piaceva.

## Mappe

Non sono riuscito a smettere di usare Google maps ma ho iniziato ad usare spesso [Organic Maps](https://organicmaps.app/it/) che usa [Open street maps](https://www.openstreetmap.org/).

Penso che sarà molto difficile usarlo al 100% viste le funzionalità di Google: notizie sul traffico, orari dei mezzi pubblici (e in alcuni casi anche in tempo reale) e le informazioni su negozi.

Il punto molto positivo di Organic Maps sono le mappe offline.

## Cronologia Google

Senza quasi neanche saperlo avevo la cronologia attiva in tutti i servizi Google: posizioni, ricerca Youtube, risultati di ricerca etc.

L'ho disabilitata ovunque e cancellato i dati precedenti per cercare di ridurre le raccomandazioni basati su posizione e dati del passato.

Mi sembra che funzioni abbastanza bene. Quando apro Youtube sul mio telefono, dove ho collegato il mio account Google, i video suggeriti nella pagina principale sono strani e non sono proprio quelli che vorrei vedere io. Mi sembrano proprio a caso.


## Facebook & Instagram

Ho ancora questi account ma ci sto lavorando.

Per ora ho messo tutti i contenuti come privati e cancellato tutte le foto (e caricate nel mio archivio foto privato).

Per il momento penso che non chiuderò gli account (forse Instagram si) perchè ogni tanto uso quello Facebook per comprare/vendere cose usate. Ha davvero un sacco di cose.

Nel frattempo ho cancellato tutte e due le applicazioni dal mio telefono e non tengo mai la sessione attiva nel browser così da non essere tentato di stare li a perdere il tempo. Quando mi serve apro una finestra temporanea in Firefox focus, faccio il login, faccio quello che devo fare e la chiudo (facendo logout).