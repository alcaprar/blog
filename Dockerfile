FROM jekyll/jekyll:3.5 as builder

WORKDIR /app

COPY Gemfile /app
COPY Gemfile.lock /app

RUN jekyll help

COPY . /app

RUN jekyll build -d public && ls

FROM node:16.14

RUN npm install --global http-server

WORKDIR /app

COPY --from=builder /app/public ./

RUN ls

EXPOSE 8080

CMD http-server .